
# $OutputPath = "c:\case\transfersub"
# $subscriptionId 

param (
[Parameter(Mandatory=$false)][string]$cloud="AzureChinaCloud",
[Parameter(Mandatory=$true)][string]$subscriptionId,
[Parameter(Mandatory=$true)][string]$OutputPath,
[Parameter(Mandatory=$false)][bool]$dumpgen2acl = $false
)


# login Azure ARM and Azure AAD
Write-Verbose "connect to Azure..." -Verbose
add-AzAccount -Environment $cloud
Set-azcontext $subscriptionId

Write-Verbose "connet to azure AD tenant..." -Verbose
Connect-AzureAD -AzureEnvironmentName $cloud

# create export folder
$foldername = "$(Get-Date -Format "yyyy-MM-dd")-$subscriptionId"
$fullpath = "$OutputPath\$foldername"
if (!(test-path $fullpath)) {
    New-Item -itemType Directory -Path $OutputPath -Name $foldername
}

$assignments = @()

Write-Verbose "Getting information about Role Assignments..." -Verbose
$roleassigments = Get-AzRoleAssignment 

foreach ($roleassigment in $roleassigments) {

    #New PSObject
    $obj = [PSCustomObject]@{
        scope = $roleassigment.scope
        DisplayName = $roleassigment.DisplayName
        SignInName = $roleassigment.SignInName
        RoleDefinitonname = $roleassigment.RoleDefinitionName
        RoleDefinitonId = $roleassigment.RoleDefinitionId
        ObjectId = $roleassigment.ObjectId
        ObjectType = $roleassigment.ObjectType
        CanDelegate = $roleassigment.CanDelegate
        Description = $roleassigment.Description
        Condition = $roleassigment.Condition
        ConditionVersion = $roleassigment.ConditionVersion    
    }

    $assignments += $obj            
 
}

$filename = "RoleAssignments.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$assignments | Export-Csv $fullPath\$filename -NoTypeInformation -Force



# dump custom user roles
$customroles = @()

Write-Verbose "Getting information about Custom Role Definiton..." -Verbose
$roles = Get-AzRoleDefinition | where {$_.iscustom -eq $true} 

foreach ($role in $roles) {

    #New PSObject
    $obj = [PSCustomObject]@{
        RoleDefinitonname = $role.name
        description = $role.description
        DefinitionId = $role.Id
        Actions = $role.Actions | ConvertTo-Json
        NotActions = $role.NotActions | ConvertTo-Json
        DataActions = $role.DataActions | ConvertTo-Json
        Assignablescopes = $role.Assignablescopes  | ConvertTo-Json   
    }

    $customroles += $obj            
 
}

$filename = "CustomRoles.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$customroles | Export-Csv $fullPath\$filename -NoTypeInformation -Force


# steps to get all MSI
Write-Verbose "Getting MSI associated with Azure Resource ..." -Verbose

$msi2resources = @()
$msis = Get-AzResource | where {$_.identity -ne $NULL -and $_.identity.type -ne 'None'} | select type, name, resourcegroupname, resourceId,  identity
foreach ($msi in $msis) {
    
    # try add systme assigned properties
    if ($msi.identity.type -match "SystemAssigned") {
        $msitype = "SystemAssigned"
        $spid = $msi.identity.PrincipalId
        $tenantId = $msi.identity.TenantId
    
        $msi2resources += [PSCustomObject]@{
            tenantId  = $tenantId
            servicesrincipalId  = $spid
            msitype = $msitype
            resourcename = $msi.name
            resourcegroupname = $msi.resourcegroupname
            resourcetype = $msi.type
            subscriptionId = $subscriptionId
        }
    } 

    # try to get user assigned identity if existing by giving resourceId
    $identity =  Get-AzResource -ResourceId $msi.resourceId
    if ($identity.identity.type -match "UserAssigned") {
        $msitype = "UserAssigned"
        foreach ($key in  $identity.identity.UserAssignedIdentities.Keys) {
            $ids = $identity.identity.UserAssignedIdentities[$key]
            $msisubID = $key.split("/")[2].tostring()

            $msi2resources += [PSCustomObject]@{
                tenantId  = $tenantId
                servicesrincipalId  = $ids.PrincipalId
                msitype = $msitype
                resourcename = $msi.name
                resourcegroupname = $msi.resourcegroupname
                resourcetype = $msi.type
                subscriptionId = $msisubID
            }

        }
    
    } 


}

$filename = "MSI.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$msi2resources | Export-Csv $fullPath\$filename -NoTypeInformation -Force


# dump storage ACLs for ADLS gen2 storage
# account should have stoage blob owner permission on target Gen2 account


if ($dumpgen2acl) {
    Write-Verbose "Getting Azure Gen2 storage account ACL ..." -Verbose
    $AzDataLakeGen2alcs = @()
    $hdfsstores = get-azstorageaccount | where {$_.EnableHierarchicalNamespace -eq $TRUE}

    foreach($hdfsstore in $hdfsstores) {
        $ctx = New-AzStorageContext -StorageAccountName $hdfsstore.StorageAccountName -UseConnectedAccount
        $containers =  Get-AzStorageContainer -Context $ctx
        foreach ($container in $containers) {

            $FileSystemName = $container.name
            $itemobj = Get-AzDataLakeGen2Item -FileSystem $FileSystemName -Context $ctx
            
            $aclcontent = @()
            $validacls = $itemobj.ACL | where {$_.EntityId -ne $NULL}
            foreach ( $validacl in $validacls) {
                $userobj = Get-AzureADObjectByObjectId -ObjectId $validacl.EntityId
                $validacl | Add-Member -MemberType NoteProperty -Name "EntityName" -Value $userobj.DisplayName -Force
                $validacl | Add-Member -MemberType NoteProperty -Name "EntityType" -Value $userobj.ObjectType -Force
                $validacl | Add-Member -MemberType NoteProperty -Name "EntityServicePrincipalType" -Value $userobj.ServicePrincipalType -Force
                $aclcontent += $validacl
            }

            # map owner
            if (  $itemobj.owner -match "superuser") {
                $ownername = $itemobj.owner
                $ownertype = "RBAC_role"
                $ownerServicePrincipalType=""
            } else {
                $userobj = Get-AzureADObjectByObjectId -ObjectId $itemobj.owner
                $ownername = $userobj.DisplayName
                $ownertype = $userobj.ObjectType
                $ownerServicePrincipalType =  $userobj.ServicePrincipalType
            }
            
            # map group
            if (  $itemobj.group -match "superuser") {
                $groupname = $itemobj.owner
                $grouptype = "RBAC_role"
                $groupServicePrincipalType=""
            } else {
                $userobj = Get-AzureADObjectByObjectId -ObjectId $itemobj.group
                $groupname = $userobj.DisplayName
                $grouptype = $userobj.ObjectType
                $groupServicePrincipalType =  $userobj.ServicePrincipalType
            }
    
            $AzDataLakeGen2alcs += [PSCustomObject]@{
                StorageAccountName= $hdfsstore.StorageAccountName
                container = $FileSystemName
                itemname = $FileSystemName
                itemtype = "container"
                path = $itemobj.path
                owner = $itemobj.owner
                ownername = $ownername 
                ownertype= $ownertype 
                ownerServicePrincipalType = $ownerServicePrincipalType
                group = $itemobj.group
                groupname = $groupname
                grouptype = $grouptype
                groupServicePrincipalType = $groupServicePrincipalType
                ACL = $aclcontent | convertto-json 
            }


            $MaxReturn = 1000
            $Total = 0
            $Token = $Null
            do
                {

                    $items = Get-AzDataLakeGen2ChildItem -FileSystem $FileSystemName -Recurse -MaxCount $MaxReturn -FetchPermission -OutputUserPrincipalName -ContinuationToken $Token -Context $ctx
                    foreach ($itemobj in $items) {
                       
                        # map owner
                        if (  $itemobj.owner -match "superuser") {
                            $ownername = $itemobj.owner
                            $ownertype = "RBAC_role"
                            $ownerServicePrincipalType=""
                        } else {
                            $userobj = Get-AzureADObjectByObjectId -ObjectId $itemobj.owner
                            $ownername = $userobj.DisplayName
                            $ownertype = $userobj.ObjectType
                            $ownerServicePrincipalType =  $userobj.ServicePrincipalType
                        }
                        
                        # map group
                        if (  $itemobj.group -match "superuser") {
                            $groupname = $itemobj.owner
                            $grouptype = "RBAC_role"
                            $groupServicePrincipalType=""
                        } else {
                            $userobj = Get-AzureADObjectByObjectId -ObjectId $itemobj.group
                            $groupname = $userobj.DisplayName
                            $grouptype = $userobj.ObjectType
                            $groupServicePrincipalType =  $userobj.ServicePrincipalType
                        }

                        if ($itemobj.IsDirectory) {
                            $itemtype = "Directory"
                        } else {
                            $itemtype = "File"
                        }

                        $aclcontent = @()
                        $validacls = $itemobj.ACL | where {$_.EntityId -ne $NULL}
                        foreach ( $validacl in $validacls) {
                            $userobj = Get-AzureADObjectByObjectId -ObjectId $validacl.EntityId
                            $validacl | Add-Member -MemberType NoteProperty -Name "EntityName" -Value $userobj.DisplayName -Force
                            $validacl | Add-Member -MemberType NoteProperty -Name "EntityType" -Value $userobj.ObjectType -Force
                            $validacl | Add-Member -MemberType NoteProperty -Name "EntityServicePrincipalType" -Value $userobj.ServicePrincipalType -Force
                            $aclcontent += $validacl
                        }
            
                        # skip recording default ACL for file/directory with default permission
                        if ($itemobj.owner -match "superuser" -and $itemobj.owner -match "superuser" -and $NULL -ne $validacls) { 
                            $AzDataLakeGen2alcs += [PSCustomObject]@{
                                StorageAccountName= $hdfsstore.StorageAccountName
                                container = $FileSystemName
                                itemname = $itemobj.name
                                itemtype = $itemtype
                                path = $itemobj.path
                                owner = $itemobj.owner
                                ownername = $ownername 
                                ownertype= $ownertype 
                                ownerServicePrincipalType = $ownerServicePrincipalType
                                group = $itemobj.group
                                groupname = $groupname
                                grouptype = $grouptype
                                groupServicePrincipalType = $groupServicePrincipalType
                                ACL = $aclcontent | convertto-json 
                            }
                        }
                    }

                    $Total += $items.Count 
                    if($items.Length -le 0) { Break;}
                    $Token = $items[$items.Count -1].ContinuationToken;
                }
            While ($null -ne $Token)
    
        }
        
    }

    $filename = "AzDataLakeGen2alcs.csv"
    Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
    $AzDataLakeGen2alcs | Export-Csv $fullPath\$filename -NoTypeInformation -Force

} else {
    Write-Verbose "skip Gen2 storage account ACL collection" -Verbose
}


# dump Azure Keyvault Access Policy 
$azkeyvaultaccesspolicies = @()
Write-Verbose "Getting Azure keyvault access policies ..." -Verbose

$azkeyvaults = get-azkeyvault
foreach ($azkeyvault in $azkeyvaults) {
    $keyVault = Get-AzKeyVault -VaultName $azkeyvault.VaultName
    $accesspolicies = $keyVault.AccessPolicies
    foreach ($accesspolicy in $accesspolicies) {
        $kvaccount = Get-AzureADObjectByObjectId -ObjectId $accesspolicy.objectId
       
        $azkeyvaultaccesspolicies+=[PSCustomObject]@{
            keyvaultname = $azkeyvault.VaultName
            ResourceGroupName=$azkeyvault.ResourceGroupName
            AccountName = $kvaccount.DisplayName
            ObjectId = $kvaccount.ObjectId
            ObjectType = $kvaccount.ObjectType
            TenantID = $accesspolicy.tenantID 
            PermissionsToKeys = $accesspolicy.PermissionsToKeys | convertto-json 
            PermissionsToSecrets = $accesspolicy.PermissionsToSecrets | convertto-json 
            PermissionsToCertificates = $accesspolicy.PermissionsToCertificates | convertto-json 
            PermissionsToStorage = $accesspolicy.PermissionsToStorage | convertto-json 
            SignInName = $kvaccount.SignInName
            AppId = $kvaccount.AppId 
            ServicePrincipalType =  $kvaccount.ServicePrincipalType
        }
    }
}

$filename = "azkeyvaultaccesspolicies.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$azkeyvaultaccesspolicies  | Export-Csv $fullPath\$filename -NoTypeInformation -Force



# dump Azure Policy Assignment and Policy Definiton
$azpolicyassignments = @()
Write-Verbose "Getting Azure policy assignments and definitons ..." -Verbose
$policyassignments = Get-AzPolicyAssignment

if (!(test-path "$fullPath\policydefinition")) {
    New-Item -itemType Directory -Path $fullPath -Name "policydefinition"
}


$custompolicysets = get-azpolicysetdefinition | where {$_.Properties.policytype -match "custom"}   

foreach ( $custompolicyset in $custompolicysets) {

    $filename = "$($custompolicyset.name).json"
    $policycontent = $custompolicyset | convertto-json -Depth 10
    $policycontent = $policycontent.replace("\u0027","'")
    Write-Verbose "Exporting policy definition file $fullPath\policydefinition\$filename" -Verbose
    $policycontent | Out-File -Encoding ASCII -Append "$fullPath\policydefinition\$filename" -Force
}

$custompolicies = get-azpolicydefinition | where {$_.Properties.policytype -match "custom"}
foreach ( $custompolicy in $custompolicies) {

    $filename = "$($custompolicy.name).json"
    $policycontent = $custompolicy | convertto-json -Depth 10
    $policycontent = $policycontent.replace("\u0027","'")
    Write-Verbose "Exporting policy definition file $fullPath\policydefinition\$filename" -Verbose
    $policycontent | Out-File -Encoding ASCII -Append "$fullPath\policydefinition\$filename" -Force
}

foreach ($policyassignment in $policyassignments) {


    $azpolicyassignments += [PSCustomObject]@{        
        name=$policyassignment.Name
        displayname=$policyassignment.Properties.DisplayName
        PolicyAssignmentId=$policyassignment.PolicyAssignmentId
        scope=$policyassignment.Properties.scope
        NotScopes= $policyassignment.Properties.NotScopes | convertto-json
        PolicyDefinitionId= $policyassignment.Properties.PolicyDefinitionId
        PolicyDefinitionName =  $policyassignment.Properties.PolicyDefinitionId.split("/")[-1].tostring()
        PolicyAssignmentproperty = $policyassignment.Properties.Parameters | convertto-json        
    }
}

$filename = "azpolicyassignments.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$azpolicyassignments  | Export-Csv $fullPath\$filename -NoTypeInformation -Force


# dump users which has assigned rbac roles in current subscription

Write-Verbose "Getting Azure AD Users details ..." -Verbose

$aadusers = $assignments | where {$_.ObjectType -match "User"} | select DisplayName, SignInName, ObjectId -Unique

$aadusermembers = @()
$aaduserroles = @()
$aaduserpermissions =@()
$aaduserlicenses = @()
$userownedapplications = @()

foreach ($aaduser in $aadusers) {
    # look AAD user memberships
 #   Write-Verbose "Getting Azure AD User memberships ..." -Verbose
    $members = Get-AzureADUserMembership -ObjectId $aaduser.ObjectId 

    $aadusergroupmembers = $members | where {$_.ObjectType -match "Group"}
    foreach ($aadusergroupmember in $aadusergroupmembers){
        # dump all user group membership if there is group based RBAC role assignment
        if ( $aadusergroupmember.objectId -in $($roleassigments | where {$_.objecttype -eq 'Group'}).objectId) {
            $groupmembers = get-azureadgroupmember -objectID $aadusergroupmember.objectId
            
            foreach ($groupmember in $groupmembers){
                $aadusermembers+=[PSCustomObject]@{
                    UserName = $groupmember.DisplayName
                    SignInName = $groupmember.UserPrincipalName
                    UserObjectId = $groupmember.ObjectId
                    GroupName = $aadusergroupmember.DisplayName
                    GroupObjectId =  $aadusergroupmember.ObjectId
                    ObjectType = $groupmember.ObjectType
                }
            }

        } else {
            $aadusermembers+=[PSCustomObject]@{
                UserName = $aaduser.DisplayName
                SignInName = $aaduser.SignInName
                UserObjectId = $adduser.ObjectId
                GroupName = $aadusergroupmember.DisplayName
                GroupObjectId =  $aadusergroupmember.ObjectId
                ObjectType = "User"
            }
        }

    }
    
 #   Write-Verbose "Getting Azure AD User roles ..." -Verbose
    $userroles = $members | where {$_.ObjectType -match "Role"}
    foreach ($userole in $userroles){

        $aaduserroles+=[PSCustomObject]@{
            UserName = $aaduser.DisplayName
            SignInName = $aaduser.SignInName
            UserObjectId = $adduser.ObjectId
            RoleName  = $userole.DisplayName
            RoleID =  $userole.ObjectId
            RoleDescription =  $userole.Description
       
        }
    }


 #   Write-Verbose "Getting Azure AD user oauth2 permissions ..." -Verbose
    
    $userpermissions = Get-AzureADUserOAuth2PermissionGrant -ObjectId $aaduser.ObjectId
    foreach ($userpermission in $userpermissions){

        $azureadresourceobj =  Get-AzureADObjectByObjectId -ObjectId $userpermission.ResourceId
      
        $aaduserpermissions+=[PSCustomObject]@{
            UserName = $aaduser.DisplayName
            SignInName = $aaduser.SignInName
            UserObjectId = $adduser.ObjectId
            Scope  = $userpermission.Scope
            ResourceId =  $userpermission.ResourceId
            ResourceName = $azureadresourceobj.DisplayName
            ResourceType = $azureadresourceobj.ObjectType      
        }
    }

    
  #  Write-Verbose "Getting Azure AD user licenses ..." -Verbose
    $userlicenses = Get-AzureADUserLicenseDetail -ObjectId $aaduser.ObjectId
    foreach ($userlicense in $userlicenses){

        $aaduserlicenses+=[PSCustomObject]@{
            UserName = $aaduser.DisplayName
            SignInName = $aaduser.SignInName
            UserObjectId = $adduser.ObjectId
            SkuId  = $userlicense.Skuid
            SkuPartNumber =  $userlicense.SkuPartNumber
        }
    }


   # Write-Verbose "Getting Azure AD user owned applications ..." -Verbose
    $ownedapplications = Get-AzureADUserOwnedObject -objectId $aaduser.ObjectId | where {$_.objectType -eq "Application"}
    foreach ($ownedapplication in $ownedapplications){

        $userownedapplications+=[PSCustomObject]@{
            UserName = $aaduser.DisplayName
            SignInName = $aaduser.SignInName
            UserObjectId = $adduser.ObjectId
            OwnedApplicationName  = $ownedapplication.DisplayName
            OwnedApplicationObjectId  =  $ownedapplication.ObjectId
            OwnedApplicationApplicationId  =  $ownedapplication.AppId
        }
    }

}


$filename = "aadusermembers.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$aadusermembers | Export-Csv $fullPath\$filename -NoTypeInformation -Force

$filename = "aaduserroles.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$aaduserroles | Export-Csv $fullPath\$filename -NoTypeInformation -Force

$filename = "aaduserpermissions.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$aaduserpermissions | Export-Csv $fullPath\$filename -NoTypeInformation -Force

$filename = "aaduserlicenses.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$aaduserlicenses | Export-Csv $fullPath\$filename -NoTypeInformation -Force

$filename = "aaduserownedapplications.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$userownedapplications | Export-Csv $fullPath\$filename -NoTypeInformation -Force


# dump service principal which has assigned rbac roles in current subscription

Write-Verbose "Getting Azure Service Principal details ..." -Verbose

$serviceprincipals = $assignments | where {$_.ObjectType -match "ServicePrincipal"} | select DisplayName, ObjectId -Unique

$aadserviceprincipals = @()
$aadserviceprincipalspermissions =@()


foreach ($serviceprincipal in $serviceprincipals) {
    $sp = Get-AzureADServicePrincipal -ObjectId $serviceprincipal.objectId 

    $sourceresourceId = ""
    $subscriptionId = ""
    $managedidentitytype = ""

    if ($sp.ServicePrincipalType -eq "ManagedIdentity") {
        foreach($msiname in $sp.AlternativeNames) {
            if ($msiname -match "/subscriptions/") {
                $sourceresourceId = $msiname
                $subscriptionId = $msiname.split("/")[2].tostring()
                if ($sourceresourceId -match 'Microsoft.ManagedIdentity/userAssignedIdentities') {
                    $managedidentitytype = "UserAssigned"
                } else {
                    $managedidentitytype = "SystemAssigned"
                }
            }

        }

    } 

    $aadserviceprincipals += [PSCustomObject]@{
        objectId=$sp.objectId
        AppId=$sp.AppId
        DisplayName=$sp.DisplayName
        ServicePrincipalType=$sp.ServicePrincipalType
        sourceresourceId=$sourceresourceId
        subscriptionId=$subscriptionId
        managedidentitytype=$managedidentitytype
    }

    $sppermissions = Get-AzureADServicePrincipalOAuth2PermissionGrant -ObjectId $serviceprincipal.objectId 
    
    foreach ($sppermission in $sppermissions){
      
        $azureadresourceobj =  Get-AzureADObjectByObjectId -ObjectId $sppermission.ResourceId

        $aadserviceprincipalspermissions+=[PSCustomObject]@{
            objectId=$sp.objectId
            AppId=$sp.AppId
            DisplayName=$sp.DisplayName
            Scope  = $sppermission.Scope
            ResourceId =  $sppermission.ResourceId
            ResourceName = $azureadresourceobj.DisplayName
            ResourceType = $azureadresourceobj.ObjectType
        }
    }
}


$filename = "aadserviceprincipals.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$aadserviceprincipals | Export-Csv $fullPath\$filename -NoTypeInformation -Force


$filename = "aadserviceprincipalspermissions.csv"
Write-Verbose "Exporting file $filename  to $fullPath" -Verbose
$aadserviceprincipalspermissions | Export-Csv $fullPath\$filename -NoTypeInformation -Force

