<#
--------------------------------------------------------------------------------------------------------------------
<copyright file="Sync-SourceControl.ps1" company="Microsoft">
Copyright (c) Microsoft Corporation.  All rights reserved.
</copyright>
--------------------------------------------------------------------------------------------------------------------
#>

<#
.SYNOPSIS 
    Runbook for source control integration. Syncs either:
        1. all runbooks from a VSTS or GitHub folder 
        2. or only runbooks correlated with a given commit or changeset id 
    with an Azure Automation account.

.DESCRIPTION
    This runbook takes a source control name and information about the user's Azure Automation account. First, the user's Run As 
    Account or Managed Identity is retrieved to login to Azure. The Bearer token is then acquired and used for authentication to get the source 
    control information via a REST API call. After this, a connection is made to either VSTS or GitHub using the 
    source control information. Then the user runbooks for the desired branch and folderPath are downloaded.
    Finally, the user runbooks are imported to the Azure Automation account via cmdlets. A summary is also printed for the user
    which displays a list of runbooks that were imported, as well as the files that were skipped (either because the file was
    not a runbook, or the size of the file was bigger than the max 1MB allowable size).

.PARAMETER SourceControlName
    The source control name associated with the Azure Automation account.

.PARAMETER AutomationAccountName
    The Azure Automation account name.

.PARAMETER ResourceGroupName
    The Resource Group name for the Azure Automation account.

.PARAMETER SourceControlAccountId
    The source control account id associated with the Azure Automation account.

.PARAMETER SourceControlJobId
    The job id that executes this script (runbook).

.PARAMETER SubscriptionId
    The subscription id for the Azure Automation account.

.PARAMETER AzureEnvironment
    Environment in which the Auzre Automation account is deployed.

.PARAMETER CommitId
    The commit (or changeset) id to which to sync source control.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $SourceControlName,

    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $AutomationAccountName,

    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $ResourceGroupName,

    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [String]
    $SourceControlAccountId=$SourceControlName,

    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [String]
    $SourceControlJobId="Sync-SourceControl",

    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $SubscriptionId,

    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [String]
    $AzureEnvironmentName="AzureChinaCloud",

    [Parameter(Mandatory=$false)]
    [String]
    $CommitId,

    [Parameter(Mandatory=$false)]
    [Hashtable]
    $TestData,

    [Parameter(Mandatory=$false)]
    [ValidateSet("Production", "LocalDevelopment", "AzurePortalDevelopment", IgnoreCase = $true)]
    [String]
    $Mode = "Production"
)

# Script settings
Set-StrictMode -Version Latest
$script:supportedRunbookTypes = @(".ps1", ".graphrunbook", ".py")
$script:sourceControlRunbookName = "Sync-SourceControl.ps1"
$script:newLine = [Environment]::NewLine
$script:userAgent = "AzureAutomationSourceControl"

# The current max file size is 1MB, which is the max runbook size in Azure Automation.
$script:maxFileSize = 1Mb

# apiEndpoint is set after the AzureEnvironment is set.
$script:apiEndpoint = $null

# The lists for downloaded files from the remote source control (VSTS or GitHub).
$script:downloadedFiles = New-Object System.Collections.Generic.List[System.Object]

# The dictionary for skipped files which will not be imported to the user's Azure Automation account.
$script:skippedFiles = @{}

# Contains all the required information about the source control. 
$script:sourceControlInfoObject = $null

# Contains all the information about the user's Azure Automation account.
$script:automationAccountInfo = $null

# The number of tries when performing webrequest calls.
$script:maxNumberOfTries = 2

#To check if Managed identity is enabled or Azure run as Account is enabled
$script:Automation_Sourcecontrol_Auth_Mechanism = $null

# Determine runbook mode and process test data if provided.
if ($Mode -eq "LocalDevelopment")
{
    if (-not $TestData)
    {
        throw "TestData hashtable must be provided in LocalDevelopment test Mode"
    }
}

if (($Mode -eq "LocalDevelopment") -or ($Mode -eq "AzurePortalDevelopment"))
{
    if ($TestData)
    {
        $requiredKeys = @("RepoUrl", "FolderPath", "SecurityToken", "TokenType", "SourceType", "PublishRunbook", "AutoSync")
        foreach ($key in $requiredKeys)
        {
            if (-not $TestData.ContainsKey($key))
            {
                throw "TestData hashtable must include '$key'."
            }
        }

        # Source Control test data
        $repoUrl                = $TestData["RepoUrl"]
        $branch                 = $TestData["Branch"]
        $folderPath             = $TestData["FolderPath"]
        $securityToken          = $TestData["SecurityToken"]
        $sourceType             = $TestData["SourceType"]
        $publishRunbook         = [System.Convert]::ToBoolean($TestData["PublishRunbook"])
        $autoSync               = [System.Convert]::ToBoolean($TestData["AutoSync"])
        $tokenType              = $TestData["TokenType"]

        $script:sourceControlInfoObject = @{
            RepoUrl = $repoUrl
            Branch = $branch
            FolderPath = $folderPath
            SecurityToken = $securityToken
            SourceType = $sourceType
            PublishRunbook = $publishRunbook
            AutoSync = $autoSync
            TokenType = $tokenType
        }
    }
}

# Set the account info object.
$script:automationAccountInfo = @{
    SourceControlName      = $SourceControlName
    AutomationAccountName  = $AutomationAccountName
    ResourceGroupName      = $ResourceGroupName
    SourceControlAccountId = $SourceControlAccountId
    SourceControlJobId     = $SourceControlJobId
    SubscriptionId         = $SubscriptionId
    AzureEnvironmentName   = $AzureEnvironmentName
    Mode                   = $Mode
}

# API version.
$script:apiVersion = "2017-05-15-preview"


# Active Directory for AzureEnvironement
$script:AzureEnvironmentToActiveDirectory = @{
    DogFood = "https://login.windows-ppe.net"
    AzureCloud = "https://login.microsoftonline.com"
    AzureGermanCloud = "https://login.microsoftonline.de"
    AzureChinaCloud = "https://login.chinacloudapi.cn"
    AzureUSGovernment = "https://login.microsoftonline.us"
}

# endpoint based on AzureEnvironmentName
$script:AzureEnvironmentToResourceManagerURL = @{
    DogFood = "https://df.onecloud.azure-test.net"
    AzureCloud = "https://management.azure.com"
    AzureGermanCloud = "https://management.microsoftazure.de"
    AzureChinaCloud = "https://management.chinacloudapi.cn"
    AzureUSGovernment = "https://management.usgovcloudapi.net"
}

#region VsoGit

function Invoke-VSOGetRestMethod
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Uri,
        
        [Parameter(Mandatory=$false)]
        [String]
        $QueryString
    )

    $requestUri = $Uri + "?api-version=1.0"

    if ($QueryString)
    {
        $requestUri = $requestUri + $QueryString
    }

    try
    {
        $headers = Set-AuthHeader -Password $script:sourceControlInfoObject.SecurityToken -TokenType $script:sourceControlInfoObject.TokenType

        $result = Invoke-RestMethod -Uri $requestUri -Headers $headers -Method Get

        # Sync all files.
        if (Has-Property -Object $result -PropertyName value)
        {
            return $result.value
        }
        # Commit Id (only changed files).
        elseif (Has-Property -Object $result -PropertyName changes) 
        {
            $listOfFiles = New-Object System.Collections.Generic.List[System.Object]

            foreach($change in $result.changes)
            {
                # Check to make sure file is in correct folder path for Source Control.
                $vsPath = Split-Path $change.item.path -Parent

                if ($vsPath.Length -ne 0)
                {
                    # FolderPaths returned from the api call need to be normalized.
                    $vsPath = Normalize-Path -Path $vsPath -SourceType VsoGit
                }
                # Are file path and source control folderPath the same (case insensitive)?
                if ($vsPath -ieq $script:sourceControlInfoObject.folderPath)
                {
                    $fileInfo = [PSCustomObject]@{
                                path = "$($change.item.path)"
                                objectID = "$($change.item.objectId)"
                                gitObjectType = "$($change.item.gitObjectType)"
                                changeIsDelete = $change.changeType -match "delete"
                            }

                    $listOfFiles.Add($fileInfo)
                }
            }
            return $listOfFiles
        }

            return $result
    }
    catch [System.Net.WebException]
    {
        $throwCustomException = $false

        # If exception is a 400 or 404
        if ($_.Exception.Message -match "40[04]")
        {
            # Get the typeKey in the exception to figure out what went wrong.
            if ($_.ErrorDetails.Message)
            {
                $messageDetails = $_.ErrorDetails.Message | ConvertFrom-Json
                $typeKey = $messageDetails.typeKey
                $errorCategory = [System.Management.Automation.ErrorCategory]::InvalidOperation

                switch ($typeKey)
                {
                    "ProjectDoesNotExistWithNameException"
                    {
                        $errorId = "InvalidProjectOrRepoName"
                        $throwCustomException = $true
                        $errorMessage = "Invalid project or repo name '{0}'." -f $UrlConnectionInformation.ProjectName
                        break
                    }

                    "GitRepositoryNotFoundException"
                    {
                        $errorId = "InvalidRepoName"
                        $throwCustomException = $true
                        $errorMessage = "Invalid repository name '{0}'." -f $UrlConnectionInformation.RepoName
                        break
                    }

                    "GitUnresolvableToCommitException"
                    {
                        $errorId = "InvalidBranchName"
                        $throwCustomException = $true
                        $errorMessage = "Invalid branch name '{0}'." -f $script:sourceControlInfoObject.Branch
                        break
                    }

                    "GitItemNotFoundException" 
                    {
                        $errorId = "InvalidFolderPath"
                        $throwCustomException = $true
                        $errorMessage = "Invalid folder path '{0}'." -f $script:sourceControlInfoObject.FolderPath
                        break
                    }

                    {"GitCommitDoesNotExistException" -or "ArgumentException"}
                    {
                        $errorId = "InvalidCommitId"
                        $throwCustomException = $true
                        $errorMessage = "Invalid commit Id '{0}'." -f $script:sourceControlInfoObject.CommitId
                        break
                    }
                }
            }
        }
        elseif ($_.Exception.Message -match "403")
        {
            $errorId = "Forbidden"
            $throwCustomException = $true
            $errorMessage = "Invalid security token."
            $errorCategory = [System.Management.Automation.ErrorCategory]::InvalidOperation
        }

        if ($throwCustomException)
        {
            Write-ErrorRecord -ErrorId $errorId `
                              -ErrorCategory $errorCategory `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
        else
        {
            throw $_
        }
    }
}

function Get-VSOGitFolderItem
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation,

        [Parameter(Mandatory=$false)]
        [String]
        $CommitId
    )

    $recurseLevel = "onelevel"
    $project = $UrlConnectionInformation.ProjectName
    $repo = $UrlConnectionInformation.RepoName
    $branch = $script:sourceControlInfoObject.Branch
    $folderPath = $script:sourceControlInfoObject.FolderPath

    $queryString = $null
    $uri = $null
    
    # If commit id, get list of changed files.
    if ($CommitId)
    {
        $uri = "https://dev.azure.com/" + $UrlConnectionInformation.AccountName +  "/$project/_apis/git/repositories/$repo/commits/$CommitId/changes"
    }
    else # This is manual sync, get all items in repo.
    {
        $uri = "https://dev.azure.com/" + $UrlConnectionInformation.AccountName + "/_apis/git/$project/repositories/$repo/items"

        $queryString = "&versionType=Branch&version=$branch&scopePath=$folderPath&recursionLevel=$recurseLevel"
    }

    $retryCount = 1
    $result = $null
    $updateSecurityToken = $false
    $lastException = $null

    do 
    {
        try
        {
            $response = Invoke-VSOGetRestMethod -Uri $uri `
                                                -QueryString $queryString `
                                                -UrlConnectionInformation $UrlConnectionInformation
            $result = VerifyResponse -Response $response -ValidationType ValidateLoginOnly
            return $result
        }
        catch [System.Net.WebException]
        {
            $lastException = $_
            if ($_.Exception.Message -match "401")
            {
                # 401 is Unauthorized. The access token might have expired, so update it.
                $updateSecurityToken = $true
            }
        }
        catch [InvalidOperationException]
        {
            $lastException = $_
            
            if ($_.Exception.Message -like "*security*token*")
            {
                $updateSecurityToken = $true
            }
        }

        if ($updateSecurityToken)
        {
            $message = "Unauthorized exception. Requesting a new source control security token."
            Write-Tracing -Level Informational -Message $message
            Update-SourceControlSecurityToken
        }

        # Update retryCount.
        $retryCount++

    } while ($retryCount -le $script:maxNumberOfTries)

    # If $result is null, re-throw the last exception.
    if ((-not $result) -and $lastException)
    {
        throw $lastException
    }
}

function Get-VSOGitFile
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $RepoID,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $BlobObjectID,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Path,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $LocalFolder
    )

    $retryCount = 1
    $fileName = Split-Path $Path -Leaf
    $localFilePath = Join-Path $LocalFolder $fileName
    $uri = "https://dev.azure.com/" + $UrlConnectionInformation.AccountName + "/" + $UrlConnectionInformation.ProjectName + "/_apis/git/repositories/$RepoID/blobs/$BlobObjectID" + "?api-version=1.0&scopePath=$Path"
    $headers = Set-AuthHeader -Password $script:sourceControlInfoObject.SecurityToken -TokenType $script:sourceControlInfoObject.TokenType
    $statusCode = 0
    $statusDescription = $null

    do
    {
        $lastException = $null
        $statusCode = 0
        $statusDescription = $null
        try
        {
            Write-Tracing -Level Informational -Message "Invoke-WebRequest -URI $uri -Headers $headers -Method Get  -outFile $localFilePath -PassThru -UseBasicParsing"
            $response = Invoke-WebRequest -Uri $uri -Headers $headers -Method Get -OutFile $localFilePath -PassThru -ErrorAction Stop -UseBasicParsing
            $statusCode = $response.StatusCode
            $statusDescription = $response.StatusDescription

            Write-Tracing -Level Informational -Message "Status code returned from Invoke-WebRequest: $statusCode"
            if ($statusCode -eq 200)
            {
                # 200 is OK
                break
            }
            elseif ($statusCode -eq 203)
            {
                # 203 is Non-Authoritative Information. The access token might have expired, so update it.
                $message = "$statusDescription ($statusCode). Requesting a new source control security token."
                Write-Tracing -Level Informational -Message $message
                Update-SourceControlSecurityToken
            }
        }
        catch [System.Net.WebException]
        {
            $statusCode = $_.Exception.Response.StatusCode.value__
            if (($statusCode -eq 401) -or ($statusCode -eq 403))
            {
                # 401 is Unauthorized; 403 is access forbidden;
                # The access token might have expired, so update it.
                $message = "Unauthorized exception (statuscode: $statusCode). Requesting a new source control security token."
                Write-Tracing -Level Informational -Message $message
                Update-SourceControlSecurityToken
            }
            else
            {
                $lastException = $_
            }
        }
        catch
        {
            $lastException = $_
        }

        # Update retryCount.
        $retryCount++

    } while ($retryCount -le $script:maxNumberOfTries)

    # If $lastException is not null, re-throw the last exception.
    if ($lastException)
    {
        throw $lastException
    }

    if (($statusCode -ne 200) -or (-not (Test-path $localFilePath -PathType Leaf)))
    {
        If ($statusDescription)
        {
            $message = "WebRequest Failed with $statusDescription($statusCode)"
        }
        else
        {
            $message = "WebRequest Failed with status code: $statusCode"
        }
        Write-ErrorRecord -ErrorId "UnknownWebRequestFailure" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($message))
    }
    
    return $localFilePath
}

function Get-VSOGitRepo
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation
    )

    $repoName = $UrlConnectionInformation.RepoName
    $projectName = $UrlConnectionInformation.ProjectName

    $uri = "https://dev.azure.com/" + $UrlConnectionInformation.AccountName + 
           "/$projectName/_apis/git/repositories/$repoName"

    $retryCount = 1
    $result = $null
    $updateSecurityToken = $false
    $lastException = $null

    do
    {
        try
        {
            $response = Invoke-VSOGetRestMethod -Uri $uri `
                                                -UrlConnectionInformation $UrlConnectionInformation
            $result = VerifyResponse -Response $response -PropertyName Id -ValidationType ValidatePropertyExists
            return $result
        }
        catch [System.Net.WebException]
        {
            $lastException = $_

            if ($_.Exception.Message -match "401")
            {
                # 401 is Unauthorized. The access token might have expired, so update it.
                $updateSecurityToken = $true
            }
        }
        catch [InvalidOperationException]
        {
            $lastException = $_

            if ($_.Exception.Message -like "*security*token*")
            {
                $updateSecurityToken = $true
            }
        }

        if ($updateSecurityToken)
        {
            $message = "Unauthorized exception. Requesting a new source control security token."
            Write-Tracing -Level Informational -Message $message
            Update-SourceControlSecurityToken
        }

        # Update retryCount.
        $retryCount++

    } while ($retryCount -le $script:maxNumberOfTries)

    # If $result is null, re-throw the last exception.
    if ((-not $result) -and $lastException)
    {
        throw $lastException
    }
}

#endregion

#region VsoTfvc

function Get-TFSVersionControlItem
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $VersionControlPath,

        [Parameter(Mandatory=$false)]
        [String]
        $ChangeSetId
    )

    $uri = $null
    $requestBody = $null
    $restMethod = $null

    # Will get list of changed files.
    if ($ChangeSetId)
    {
        $restMethod = "Get"
        $uri = "https://dev.azure.com/" + $UrlConnectionInformation.AccountName + "/_apis/tfvc/changesets/$ChangeSetId/changes"
    }
    # Will get list of all files.
    else 
    {
        $restMethod = "Post"
        $uri = "https://dev.azure.com/" + $UrlConnectionInformation.AccountName + "/_apis/tfvc/itemBatch"
        $requestBody = @"
{
  "itemDescriptors": [
    {
      "path": "$VersionControlPath",
      "recursionLevel": "onelevel"
    }
  ]
}
"@
    }

    # Set the API version.
    $uri = $uri + "?api-version=1.0"

    $retryCount = 1
    $result = $null
    $updateSecurityToken = $false
    $lastException = $null

    do
    {
        try
        {
            $response = Invoke-TFSRestMethod -Uri $uri `
                                             -UrlConnectionInformation $UrlConnectionInformation `
                                             -RestMethod $restMethod `
                                             -RequestBody $requestBody `
                                             -ChangeSetId $ChangeSetId
            if ($ChangeSetId)
            {
                $result = VerifyResponse -Response $response -PropertyName value -ValidationType ValidateAndReturnProperty

                $listOfFiles = New-Object System.Collections.Generic.List[System.Object]

                # Get the properties we want from each changed file object.
                foreach($change in $result)
                {
                    # for rollback case, the changetype is "undelete, rollback"
                    # consider change type as deleted only when a work delete is found in the string
                    $fileInfo = [PSCustomObject]@{
                                path = "$($change.item.path)"
                                changeIsDelete = $change.changeType -match "\bdelete"
                    }
                    $listOfFiles.Add($fileInfo)
                }

                return $listOfFiles
            }

            $result = VerifyResponse -Response $response -PropertyName value -ValidationType ValidateAndReturnProperty
            return $result
        }
        catch [System.Net.WebException]
        {
            $lastException = $_

            if ($_.Exception.Message -match "401")
            {
                # 401 is Unauthorized. The access token might have expired, so update it.
                $updateSecurityToken = $true
            }
        }
        catch [InvalidOperationException]
        {
            $lastException = $_

            if ($_.Exception.Message -like "*security*token*")
            {
                $updateSecurityToken = $true
            }
        }

        if ($updateSecurityToken)
        {
            $message = "Unauthorized exception. Requesting a new source control security token."
            Write-Tracing -Level Informational -Message $message
            Update-SourceControlSecurityToken
        }

        # Update retryCount.
        $retryCount++

    } while ($retryCount -le $script:maxNumberOfTries)

    # If $result is null, re-throw the last exception.
    if ((-not $result) -and $lastException)
    {
        throw $lastException
    }
}

function Invoke-TFSRestMethod
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Uri,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation,

        [Parameter(Mandatory=$true)]
        [String]
        $RestMethod,

        [Parameter(Mandatory=$false)]
        [String]
        $RequestBody,

        [Parameter(Mandatory=$false)]
        [String]
        $ChangeSetId
    )

    $headers = Set-AuthHeader -Password $script:sourceControlInfoObject.SecurityToken -TokenType $script:sourceControlInfoObject.TokenType

    try
    {
        $result = $null

        if ($RequestBody)
        {
            $result = Invoke-RestMethod -Uri $Uri -Headers $headers -Method $RestMethod -Body $RequestBody -ContentType "application/json"
        }
        else
        {
            $result = Invoke-RestMethod -Uri $Uri -Headers $headers -Method $RestMethod -ContentType "application/json"       
        }

        return $result
    }
    catch [System.Net.WebException]
    {
        $throwCustomException = $false

        # If exception is a 400 or 404
        if ($_.Exception.Message -match "40[04]")
        {
            # 400 is 'Bad Request', so let's get more details on the failure.
            if ($_.ErrorDetails.Message)
            {
                # Get the typeKey in the exception to figure out what went wrong.
                $MessageDetails = $_.ErrorDetails.Message | ConvertFrom-Json
                if ($MessageDetails.typeKey -eq "ItemBatchNotFoundException")
                {
                    $errorId = "InvalidFolderPathOrProjectName"
                    $throwCustomException = $true
                    $errorMessage = "Project name '{0}' or folder path '{1}' not found." -f $UrlConnectionInformation.ProjectName, $VersionControlPath
                    $errorCategory = [System.Management.Automation.ErrorCategory]::InvalidOperation

                }
                elseif ($MessageDetails.typeKey -eq "ChangesetNotFoundException")
                {
                    $errorId = "InvalidChangesetId"
                    $throwCustomException = $true
                    $errorMessage = "Invalid changeset Id '{0}'." -f $ChangeSetId
                    $errorCategory = [System.Management.Automation.ErrorCategory]::InvalidOperation 
                }
                elseif ($MessageDetails.typeKey -eq "InvalidVersionSpecException")
                {
                    $errorId = "InvalidChangesetNumber"
                    $throwCustomException = $true
                    $errorMessage = "'{0}' is not a valid changeset number." -f $ChangeSetId
                    $errorCategory = [System.Management.Automation.ErrorCategory]::InvalidOperation 
                }
            }
        }
        elseif ($_.Exception.Message -match "401")
        {
            $errorId = "Unauthorized"
            $throwCustomException = $true
            $errorMessage = "Unauthorized, access is denied."
            $errorCategory = [System.Management.Automation.ErrorCategory]::AuthenticationError
        }

        if ($throwCustomException)
        {
            Write-ErrorRecord -ErrorId $errorId `
                              -ErrorCategory $errorCategory `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
        else
        {
            throw $_
        }
        }
}

function Get-TFSVersionFile
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $VersionControlPath,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $LocalFolder
    )

    $headers = Set-AuthHeader -Password $script:sourceControlInfoObject.SecurityToken `
                              -TokenType $script:sourceControlInfoObject.TokenType

    $uri = "https://dev.azure.com/" + $UrlConnectionInformation.AccountName + "/_apis/tfvc/items/$VersionControlPath"
    $uri = $uri + "?api-version=1.0"

    $retryCount = 1
    $fileName = Split-Path $VersionControlPath -Leaf
    $localFilePath = Join-Path $LocalFolder $fileName
    $statusCode = 0
    $statusDescription = $null

    do
    {
        $lastException = $null
        $statusCode = 0
        $statusDescription = $null
        try
        {
            Write-Tracing -Level Informational -Message "Invoke-WebRequest -URI $uri -Headers $headers -Method Get  -outFile $localFilePath -PassThru -UseBasicParsing"
            $response = Invoke-WebRequest -Uri $uri -Headers $headers -Method Get -OutFile $localFilePath -PassThru -ErrorAction Stop -UseBasicParsing
            $statusCode = $response.StatusCode
            $statusDescription = $response.StatusDescription

            Write-Tracing -Level Informational -Message "Status code returned from Invoke-WebRequest: $statusCode"
            if ($statusCode -eq 200)
            {
                # 200 is OK
                break
            }
            elseif ($statusCode -eq 203)
            {
                # 203 is Non-Authoritative Information. The access token might have expired, so update it.
                $message = "$statusDescription ($statusCode). Requesting a new source control security token."
                Write-Tracing -Level Informational -Message $message
                Update-SourceControlSecurityToken
            }
        }
        catch [System.Net.WebException]
        {
            $statusCode = $_.Exception.Response.StatusCode.value__
            Write-Tracing -Level Informational -Message "Status code in exception from Invoke-WebRequest: $statusCode"
            if (($statusCode -eq 401) -or ($statusCode -eq 403))
            {
                # 401 is Unauthorized; 403 is access forbidden;
                # The access token might have expired, so update it.
                $message = "Unauthorized exception. Requesting a new source control security token."
                Write-Tracing -Level Informational -Message $message
                Update-SourceControlSecurityToken
            }
            else
            {
                $lastException = $_
            }
        }
        catch
        {
            $lastException = $_
        }

        # Update retryCount.
        $retryCount++

    } while ($retryCount -le $script:maxNumberOfTries)

    # If $lastException is not null, re-throw the last exception.
    if ($lastException)
    {
        throw $lastException
    }

    if (($statusCode -ne 200) -or (-not (Test-path $localFilePath -PathType Leaf)))
    {
        If ($statusDescription)
        {
            $message = "WebRequest Failed with $statusDescription($statusCode)"
        }
        else
        {
            $message = "WebRequest Failed with status code: $statusCode"
        }
        Write-ErrorRecord -ErrorId "UnknownWebRequestFailure" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($message))
    }

    return $localFilePath
}

#endregion

#region GitHub

<# LIMITATIONS:
- 5000 requests per hour (cannot load more than 4995 files per hour - the validation calls use up 5 calls).
- Cannot have more than 500,000 total nodes per call (so when listing all files in a folderPath,
  there cannot be more than 500,000 files).
- All variables for API calls are case sensitive. User must provide the correct casing for information.
#>
function Invoke-GitHubPostRestMethod
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $RequestBody
    )

    $uri = "https://api.github.com/graphql"
    $headers = Set-AuthHeader -Password $script:sourceControlInfoObject.SecurityToken `
                              -TokenType $script:sourceControlInfoObject.TokenType `
                              -SourceType GitHub

    try
    {
        $response = Invoke-RestMethod -Method POST -Uri $uri -Headers $headers -ContentType "application/json" -Body $RequestBody
        return $response
    }
    catch [System.Net.WebException]
    {
        $throwCustomException = $false
        if ($_.Exception.Message -match "401")
        {
            $errorId = "Unauthorized"
            $throwCustomException = $true
            $errorCategory = [System.Management.Automation.ErrorCategory]::AuthenticationError
            $errorMessage = "Unauthorized, access is denied."
        }
        elseif ($_.Exception.Message -match "403")
        {
            $errorId = "Forbidden"
            $throwCustomException = $true
            $errorCategory = [System.Management.Automation.ErrorCategory]::InvalidOperation
            $errorMessage = "Invalid security token."
        }

        if ($throwCustomException)
        {
            Write-ErrorRecord -ErrorId $errorId `
                              -ErrorCategory $errorCategory `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
        else
        {
            throw $_
        }
    }
}

# This function returns a response object from an invoke-restmethod call to GitHub.
# If the file is too large (bigger than 1 Mb), return null.
#
function Invoke-GitHubGetRestMethod
{
    Param 
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $AccountName,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $RepoName,

        [Parameter(Mandatory=$false)]
        [String]
        $Branch,

        [Parameter(Mandatory=$false)]
        [String]
        $FolderPath,

        [Parameter(Mandatory=$false)]
        [String]
        $FileName,

        [Parameter(Mandatory=$false)]
        [String]
        $CommitId
    )

    # Get commit details.
    if ($CommitId)
    {
        $uri = @"
https://api.github.com/repos/$AccountName/$RepoName/commits/{0}
"@ -f $CommitId  
    }
    # Get file contents.
    else
    {
        $uri = @"
https://api.github.com/repos/$AccountName/$RepoName/contents/{0}/{1}?ref={2}
"@ -f $FolderPath, $FileName, $Branch
    }

    $headers = Set-AuthHeader -Password $script:sourceControlInfoObject.SecurityToken `
                              -TokenType $script:sourceControlInfoObject.TokenType `
                              -SourceType GitHub `
                              -GetFileContents

    try
    {
        $response = Invoke-RestMethod -Method Get -Uri $uri -Headers $headers -ContentType "application/json"
        return $response
    }
    catch [System.Net.WebException]
    {
        $throwCustomException = $false
        if ($_.Exception.Message -match "401")
        {
            $errorId = "Unauthorized"
            $throwCustomException = $true
            $errorCategory = [System.Management.Automation.ErrorCategory]::AuthenticationError
            $errorMessage = "Unauthorized, access is denied."
        }
        elseif ($_.Exception.Message -match "403")
        {
            if ($_.ErrorDetails.Message -match "too_large")
            {
                return
            }
            else
            {
                $errorId = "Forbidden"
                $throwCustomException = $true
                $errorCategory = [System.Management.Automation.ErrorCategory]::InvalidOperation
                $errorMessage = "Invalid security token."
            }
        }
        elseif (($_.Exception.Message -match "404") -or ($_.Exception.Message -match "422"))
        {
            $errorId = "NotFound"
            $throwCustomException = $true
            $errorCategory = [System.Management.Automation.ErrorCategory]::InvalidOperation

            if ($CommitId)
            {
                $errorMessage = "Invalid commit Id '{0}'." -f $CommitId
            }
            else
            {
                $errorMessage = "Resource not found '{0}'." -f $uri
            }
        }

        if ($throwCustomException)
        {
            Write-ErrorRecord -ErrorId $errorId `
                              -ErrorCategory $errorCategory `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
        else
        {
            throw $_
        }
    }
}

function New-RequestBody
{
    Param 
    (
        [Parameter(Mandatory=$false)]
        [String]
        $Branch,

        [Parameter(Mandatory=$false)]
        [String]
        $Path,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $RepoName,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $AccountName
    )

    $requestBody = $null

    if ([String]::IsNullOrWhiteSpace($Branch) -and [String]::IsNullOrWhiteSpace($Path))
    {
        $requestBody = @"
{{"query":"query {{repository(name:\"{0}\",owner:\"{1}\"){{object(expression:\"\"){{...on Tree{{entries{{name,type}}}}}}}}}}"}}
"@ -f $RepoName, $AccountName
    }
    else
    {
        $requestBody = @"
{{"query":"query {{repository(name:\"{0}\",owner:\"{1}\"){{object(expression:\"{2}:{3}\"){{...on Tree{{entries{{name,type}}}}}}}}}}"}}
"@ -f $RepoName, $AccountName, $Branch, $Path
    }

    return $requestBody
}

function Get-GitHubListOfFilesToSync
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation,

        [Parameter(Mandatory=$true)]
        [AllowEmptyString()]
        [String]
        $FolderPath,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Branch,

        [Parameter(Mandatory=$false)]
        [String]
        $CommitId
    )

    # Ensure that account name (user name), security token, and repo name are valid.
    $requestBody = New-RequestBody -RepoName $UrlConnectionInformation.RepoName `
                                   -AccountName $UrlConnectionInformation.AccountName

    $response = Invoke-GitHubPostRestMethod -RequestBody $requestBody

    if (-not $response.data.repository) 
    {
        # Either the RepoName or AccountName are invalid. The error message is available at 
        # $response.data.errors, e.g.: 
        # "message": "Could not resolve to a Repository with the name '{RepoName}'",
        # "message": "Could not resolve to a User with the username '{AccountName}'."
        if ($response.errors[0].message)
        {
            $errorMessage = $response.errors[0].message
        }
        else
        {
            $errorMessage = "Either RepoName or UserName is invalid."
        }

        Write-ErrorRecord -ErrorId "GitHubRepoNameOrUserNameNotValid" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::AuthenticationError) `
                          -Exception ([System.InvalidOperationException]::New($errorMessage))
    }

    # Validate the branch.
    $requestBody = New-RequestBody -RepoName $UrlConnectionInformation.RepoName `
                                   -AccountName $UrlConnectionInformation.AccountName `
                                   -Branch $Branch `
                                   -Path $FolderPath

    $response = Invoke-GitHubPostRestMethod -RequestBody $requestBody

    if (-not $response.data.repository.object) 
    {
        <# When the branch does not exist, the repository's object property will be null.
        "data": {
            "repository": {
                "object": null
            }
        #>
        $errorMessage = "Branch '{0}' does not exist." -f $Branch
        Write-ErrorRecord -ErrorId "GitHubBranchDoesNotExist" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidArgument) `
                          -Exception ([System.InvalidOperationException]::New($errorMessage))
    }

    # Validate the FolderPath. Please note that if this call succeeds, then we also get the list of files.
    $requestBody = New-RequestBody -RepoName $UrlConnectionInformation.RepoName `
                                   -AccountName $UrlConnectionInformation.AccountName `
                                   -Branch $Branch `
                                   -Path $FolderPath

    $response = Invoke-GitHubPostRestMethod -RequestBody $requestBody

    if (-not $response.data.repository.object) 
    {
        # When the folderPath does not exist, the repository's object property will be null.
        $errorMessage = "FolderPath '{0}' does not exist." -f $FolderPath
        Write-ErrorRecord -ErrorId "GitHubFolderPathDoesNotExist" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidArgument) `
                          -Exception ([System.InvalidOperationException]::New($errorMessage))

    }

    # At this point, the accountName, repoName, securityToken, branch, and folderPath are valid.

    # Get list of files changed from commit id.
    if ($CommitId)
    {
        $commitDetails = Invoke-GitHubGetRestMethod -AccountName $UrlConnectionInformation.AccountName `
                                                    -RepoName $UrlConnectionInformation.RepoName `
                                                    -CommitId $CommitId

        $listOfFiles = New-Object System.Collections.Generic.List[System.Object]

        foreach($fileObject in $commitDetails.files)
        {
            # Check to make sure file is in correct folder path for Source Control.
            $gitHubPath = Split-Path $fileObject.filename -Parent
            
            if ($gitHubPath.Length -gt 0)
            {
                $gitHubPath = Normalize-Path -Path $gitHubPath -SourceType GitHub
            }
            # Are file path and source control folderPath the same (case insensitive)?
            if ($gitHubPath -ieq $FolderPath)
            {
                $fileInfo = [PSCustomObject]@{
                    name = Split-Path $fileObject.filename -Leaf
                    type = "blob"
                    changeIsDelete = $fileObject.status -match "removed"
                }

                $listOfFiles.Add($fileInfo)
            }
        }

        return $listOfFiles
    }
    # Return list of all files in source control.
    # $response.data.repository.object.entries from above will include the list of files.
    return $response.data.repository.object.entries
}

#endregion

#region Helper functions

function Write-Tracing
{
    Param
    (
        [Parameter(Mandatory=$true)]      
        [ValidateSet("Informational", "Warning", "ErrorLevel", "Succeeded", IgnoreCase = $true)]
        [String]
        $Level,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Message,

        [Switch]
        $DisplayMessageToUser
    )

    Write-Output $Message

}

function ValidateRequiredKeys
{
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [String[]]
        $RequiredKeys
    )

    foreach ($keyName in $RequiredKeys)
    {
        if (-not $script:automationAccountInfo.ContainsKey($keyName))
        {
            $errorMessage = "$keyName is required to retrieve the Azure Automation Source Control."
            Write-ErrorRecord -ErrorId "$($keyName)IsRequired" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
    }
}


function Get-SourceControlSecurityTokenProperties
{
    Param
    (
        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [String]
        $AcessToken
    )

    # Validate that the required values to retrieve the ARM Source Control Object properties.
    ValidateRequiredKeys `
        -RequiredKeys @("SourceControlName", "SubscriptionId", "ResourceGroupName", "AutomationAccountName")
    
    $sourceControlName     = $script:automationAccountInfo["SourceControlName"]
    $subscriptionId        = $script:automationAccountInfo["SubscriptionId"]
    $resourceGroupName     = $script:automationAccountInfo["ResourceGroupName"]
    $automationAccountName = $script:automationAccountInfo["AutomationAccountName"]

    if (-not $AcessToken)
    {
        # The ARM access token is not present, then retrieve a new one.
        $AcessToken = Get-AccessToken
    }

    $requestHeader = @{
      "Authorization" = "Bearer {0}" -f $AcessToken
      "Content-Type" = "application/json"
    }   

    # Request uri.
    $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/sourceControls/$sourceControlName/listKeys`?api-version=$script:apiVersion"

    try
    {
        $result = Invoke-RestMethod -Method Post -Headers $requestheader -Uri $uri -ErrorAction Stop

        if (-not $result.securityToken)
        {
            $errorMessage = "response does not contain 'securityToken'"
            Write-ErrorRecord -ErrorId "SecurityTokenNotFound" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }

        if (-not $result.tokenType)
        {
            $errorMessage = "response does not contain 'tokenType'"
            Write-ErrorRecord -ErrorId "TokenTypeNotFound" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }

        return $result
    }
    catch
    {
        $errorMessage = "Failed to get security token via listKeys."
        $errorMessage += $script:newLine + "Uri: $uri"
        $errorMessage = New-ErrorMessage -ExceptionThrown $_ -Message $errorMessage

        Write-Tracing -Level ErrorLevel -Message $errorMessage

        Write-ErrorRecord -ErrorId "SecurityTokenNotFound" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($errorMessage))
    }
}
function Find-IfManagedIdentityHasAccessToAutomationAccount
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $AccessToken,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $MSIType
    )
    try
    {
        # Validate that the required values to retrieve the ARM Source Control Object properties.
        ValidateRequiredKeys `
            -RequiredKeys @("SourceControlName", "SubscriptionId", "ResourceGroupName", "AutomationAccountName")

        $sourceControlName     = $script:automationAccountInfo["SourceControlName"]
        $subscriptionId        = $script:automationAccountInfo["SubscriptionId"]
        $resourceGroupName     = $script:automationAccountInfo["ResourceGroupName"]
        $automationAccountName = $script:automationAccountInfo["AutomationAccountName"]

        $requestHeader = @{
        "Authorization" = "Bearer {0}" -f $AccessToken
        "Content-Type" = "application/json"
        }
        $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/sourceControls/$sourceControlName/listKeys`?api-version=$script:apiVersion"
        Invoke-RestMethod -Method Post -Headers $requestheader -Uri $uri -ErrorAction Stop | out-null
        $script:Automation_Sourcecontrol_Auth_Mechanism = $MSIType
    }
    catch
    {
        $StatusCode = $_.Exception.Response.StatusCode.value__
        if($StatusCode -eq 403)
        {  
            if($MSIType -eq "User-Assigned Managed Identity")
            {
                $script:Automation_Sourcecontrol_Auth_Mechanism = $MSIType
                throw
            }
            else
            {
                Write-Tracing -Level Informational -message "$MSIType is enabled but it does not have Contributor access to Automation account" -DisplayMessageToUser 
            }
        }
    }
}
function Get-ManagedIdentityAccessToken
{
    Param
    (
        [Parameter(Mandatory=$false)]
        [String]
        $ClientId
    )
    $resource = "?resource=$script:apiEndpoint"
    $url = $env:IDENTITY_ENDPOINT + $resource
    if($ClientId)
    {
        $url += "&client_id=$ClientId"
    }
    $Headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]" 
    $Headers.Add("Metadata", "True")
    $Headers.Add("X-IDENTITY-HEADER", $env:IDENTITY_HEADER)
    $Response = Invoke-RestMethod -Uri $url -Method 'GET' -Headers $Headers -ErrorAction Stop
    return $Response
}
function Get-AuthenticationMechanismForRunbook
{
    $ClientId = Get-AutomationVariable -Name "AUTOMATION_SC_USER_ASSIGNED_IDENTITY_ID" -ErrorAction SilentlyContinue
    if($ClientId)
    {
        try
        {
            $Response = Get-ManagedIdentityAccessToken -ClientId $ClientId
            Find-IfManagedIdentityHasAccessToAutomationAccount -AccessToken $Response.access_token `
                                                               -MSIType "User-Assigned Managed Identity"
        }
        catch
        {  
            if($script:Automation_Sourcecontrol_Auth_Mechanism -eq "User-Assigned Managed Identity")
            {
                $errorMessage = "$script:Automation_Sourcecontrol_Auth_Mechanism is enabled but it does not have Contributor access to Automation account"
            }
            else 
            {
                $StatusCode = $_.Exception.Response.StatusCode.value__
                $stream = $_.Exception.Response.GetResponseStream()
                $reader = New-Object System.IO.StreamReader($stream)
                $responseBody = $reader.ReadToEnd()
                if(400 -eq $StatusCode)
                {
                    $errorMessage = "Re-check the value of variable AUTOMATION_SC_USER_ASSIGNED_IDENTITY_ID with client ID. "
                    $errorMessage += "Ensure User-Assigned Managed Identity is enabled and provide contributor access to automation account to use it with source control." 
                }
                else
                {
                    $errorMessage = "User-Assigned Managed identity is enabled but not able to retrieve access token at this moment."
                    $errorMessage += "Request Failed with Status: $StatusCode, Message: $responseBody"
                }
            }
            $exception = [System.InvalidOperationException]::New($errorMessage)
            Write-ErrorRecord -ErrorId "UnableToRetrieveAcessToken" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception $exception
        }
    }
    elseif((Get-AutomationVariable -Name "AUTOMATION_SC_USE_RUNAS" -ErrorAction SilentlyContinue) -ne $true)
    {
        try
        {
            $Response = Get-ManagedIdentityAccessToken
            Find-IfManagedIdentityHasAccessToAutomationAccount -AccessToken $Response.access_token `
                                                               -MSIType "System-Assigned Managed Identity"
        }
        catch
        {
            $StatusCode = $_.Exception.Response.StatusCode.value__
            $stream = $_.Exception.Response.GetResponseStream()
            $reader = New-Object System.IO.StreamReader($stream)
            $responseBody = $reader.ReadToEnd()
            if(400 -eq $StatusCode)
            {
                $message = "If User-Assigned Managed Identity is enabled, add variable AUTOMATION_SC_USER_ASSIGNED_IDENTITY_ID with client id. "
                $message += "System-Assigned Managed Identity is not Enabled. Enable Managed Identity and provide contributor access to automation account to use it with source control. "
                Write-Tracing -Level Informational -message $message -DisplayMessageToUser 
            }
            else
            {
                $errorMessage = "Not able to retrieve access token at this moment."
                $errorMessage += " Request Failed with Status: $StatusCode, Message: $responseBody"
                $exception = [System.InvalidOperationException]::New($errorMessage)
                Write-ErrorRecord -ErrorId "UnableToRetrieveAcessToken" `
                                  -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                  -Exception $exception `
                                  -Type NonTerminatingError
            }
        }
    }
    if(-not $script:Automation_Sourcecontrol_Auth_Mechanism)
    {
        try
        {
            Get-AutomationConnection -Name "AzureRunAsConnection" -ErrorAction Stop | out-null
            $script:Automation_Sourcecontrol_Auth_Mechanism = "RunAsAccount" 
        }
        catch 
        {
            $errorMessage = "Unable to retrieve Managed Identity/ AzureRunAsConnection." + $script:newLine
            $errorMessage += "Enable managed identity. Managed identity would be more secure than Run as Account and"
            $errorMessage += " offers ease of use since it doesn’t require any credentials to be stored."+ $script:newLine
            $errorMessage += "In case Managed Identity is already enabled, give contributor access in the automation account." + $script:newLine
            $errorMessage += "Relevant Doc: https://docs.microsoft.com/azure/automation/source-control-integration#prerequisites" +$script:newLine
            $errorMessage += " You can also create Run As Account, validate AzureRunAsConnection exist and retry." + $script:newLine   
            $errorMessage += "Relevant Doc: https://docs.microsoft.com/en-us/azure/automation/automation-create-runas-account#create-run-as-account-from-the-portal"
            $exception = [System.InvalidOperationException]::New($errorMessage)
            Write-ErrorRecord -ErrorId "UnableToRetrieveAzureRunAsConnectionorManagedIdentity" `
                                -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                -Exception $exception
        }
    }   
}
function Set-AzureAutomationSourceControl
{
    # Validate that the required values to retrieve the ARM Source Control Object properties.
    ValidateRequiredKeys `
        -RequiredKeys @("SourceControlName", "SubscriptionId", "ResourceGroupName", "AutomationAccountName")

    $sourceControlName     = $script:automationAccountInfo["SourceControlName"]
    $subscriptionId        = $script:automationAccountInfo["SubscriptionId"]
    $resourceGroupName     = $script:automationAccountInfo["ResourceGroupName"]
    $automationAccountName = $script:automationAccountInfo["AutomationAccountName"]

    # Get the ARM access  token to authenticate with Azure.
    $acessToken = Get-AccessToken

    $requestHeader = @{
      "Authorization" = "Bearer {0}" -f $AcessToken
      "Content-Type" = "application/json"
    }

    $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/sourceControls/$sourceControlName`?api-version=$script:apiVersion"

    # Get the source control object.
    try
    {
        $result = Invoke-RestMethod -Method Get -Headers $requestheader -Uri $uri -ErrorAction Stop

        $script:sourceControlInfoObject = @{
            Name = $result.name
            RepoUrl = $result.properties.repoUrl
            Branch = $result.properties.branch
            FolderPath = $result.properties.folderPath
            SourceType = $result.properties.sourceType
            AutoSync = [System.Convert]::ToBoolean($result.properties.autoSync)
            PublishRunbook = [System.Convert]::ToBoolean($result.properties.publishRunbook)
        }
    }
    catch
    {
        $errorMessage = "Failed to get Azure Automation Source Control via REST API."
        $errorMessage += $script:newLine + "Uri: $uri"
        $errorMessage = New-ErrorMessage -ExceptionThrown $_ -Message $errorMessage

        Write-Tracing -Level ErrorLevel -Message $errorMessage

        Write-ErrorRecord -ErrorId "SourceControlObjectNotFound" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($errorMessage))

    }

    # Get the security token.
    $securityTokenProperties = Get-SourceControlSecurityTokenProperties -AcessToken $AcessToken

    $script:sourceControlInfoObject["SecurityToken"] = $securityTokenProperties.securityToken
    $script:sourceControlInfoObject["TokenType"]     = $securityTokenProperties.tokenType
}

function Update-SourceControlSecurityToken
{
    # Get the security token.
    $securityTokenProperties = Get-SourceControlSecurityTokenProperties

    # Update the sourceControlInfoObject with the new SecurityToken and Token Type
    $script:sourceControlInfoObject["SecurityToken"] = $securityTokenProperties.securityToken
    $script:sourceControlInfoObject["TokenType"]     = $securityTokenProperties.tokenType
}



function Set-AuthHeader
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]$Password,

        [Parameter(Mandatory=$true)]
        [ValidateSet("PersonalAccessToken", "Oauth", IgnoreCase = $true)]
        [String]
        $TokenType,

        [Parameter(Mandatory=$false)]
        [ValidateSet("VsoGit", "VsoTfvc", "GitHub", IgnoreCase = $true)]
        [String]
        $SourceType,

        [Switch]
        $GetFileContents
    )

    if ($SourceType -eq "GitHub")
    {
        $basicAuthHeader = @{
            "Authorization" = "token ${Password}"
            "User-Agent" = $script:userAgent
        }

        if ($GetFileContents)
        {
            # Specify GitHub api v.3
            $basicAuthHeader["Accepts"] = "application/vnd.github.v3+json"
        }
    }
    else # VSTS
    {
        if ($TokenType -eq "PersonalAccessToken")
        {
            $vsAuthCredential =  ":" + $Password
            $vsAuth = [System.Text.Encoding]::UTF8.GetBytes($vsAuthCredential)
            $vsAuth = [System.Convert]::ToBase64String($vsAuth)
            $basicAuthHeader = @{Authorization="Basic ${vsAuth}"}
        }
        elseif ($TokenType -eq "Oauth")
        {
            $basicAuthHeader = @{
              "Authorization" = "Bearer ${Password}"
              "Content-Type" = "application/json"
            }
        }
    }

    return $basicAuthHeader
}

function Get-SourceControlFile
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [Hashtable]
        $UrlConnectionInformation,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $LocalFolder,

        [Parameter(Mandatory=$false)]
        [String]
        $CommitId
    )

    # In VsoTfvc, 'branch' does not exist.
    if ($script:sourceControlInfoObject.SourceType -ne "VsoTfvc")
    {
        if (-not $script:sourceControlInfoObject.Branch)
        {
            $errorMessage = "Branch is required for SourceType 'VsoGit' and 'GitHub'."
            Write-ErrorRecord -ErrorId "BranchRequiredForVsoGitAndGitHub" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidArgument) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
    }

    # Normalize folder path.
    $script:sourceControlInfoObject.FolderPath = Normalize-Path -Path $script:sourceControlInfoObject.FolderPath `
                                                                -SourceType $script:sourceControlInfoObject.SourceType

    $downloadedFiles = New-Object System.Collections.Generic.List[System.Object]

    $toBeDeletedFiles = New-Object System.Collections.Generic.List[System.Object]

    if ($script:sourceControlInfoObject.SourceType -eq "VsoGit")
    {
        # Get the repo id, which is needed to request the list of files in the branch and folder.
        $repoInformation = Get-VSOGitRepo -UrlConnectionInformation $UrlConnectionInformation

        # Get the list of files.
        Write-Tracing -Level Informational -Message "Get the list of files in the folder to sync."
        $filesToSync = @(Get-VSOGitFolderItem -UrlConnectionInformation $UrlConnectionInformation `
                                              -CommitId $CommitId)

        foreach ($file in $filesToSync)
        { 
            if ($file.gitObjectType -eq "blob")
            {
                $fileName = Split-Path $file.path -Leaf

                if (ShouldProcessFile -FileName $fileName)
                {
                    # If the file's change is 'delete' add to delete list.
                    if ((Has-Property -Object $file -PropertyName changeIsDelete) -and $file.changeIsDelete)
                    {
                       $toBeDeletedFiles.Add($fileName) 
                    }
                    # Download file.
                    else 
                    {
                        try
                        {
                            Write-Tracing -Level Informational -Message "Downloading $fileName..."
                            $filePath = Get-VSOGitFile -UrlConnectionInformation $UrlConnectionInformation `
                                                       -RepoID $repoInformation.id `
                                                       -BlobObjectID $file.objectID `
                                                       -Path $file.path `
                                                       -LocalFolder $LocalFolder

                            $downloadedFiles.Add($filePath)
                            Write-Tracing -Level Succeeded -Message "File '$fileName' downloaded successfully."
                        }
                        catch
                        {
                            Write-Tracing -Level ErrorLevel -Message "Failed to download file '$fileName'. Exception: $($_.Exception)"
                            Add-FileToSkip -FileName $fileName -Reason FailedToDownloadFile
                        } 
                    }
                }
            }
        }
    }
    elseif ($script:sourceControlInfoObject.SourceType -eq "VsoTfvc")
    {
        $versionControlPath = '$/' + $UrlConnectionInformation.ProjectName + $folderPath
        $filesToSync = @(Get-TFSVersionControlItem -UrlConnectionInformation $UrlConnectionInformation `
                                                   -VersionControlPath $versionControlPath `
                                                   -ChangeSetId $CommitId)

        foreach ($file in $filesToSync)
        {
            # In VsoTfvc the only way to figure out if we are processing a file is
            # to filter out if it is a folder.
            if (-not (Has-Property -Object $file -PropertyName isFolder))
            {
                $fileName = Split-Path $file.path -Leaf

                if (ShouldProcessFile -FileName $fileName)
                {
                    # If the file's change is 'delete' add to delete list.
                    if ((Has-Property -Object $file -PropertyName changeIsDelete) -and $file.changeIsDelete)
                    {
                        $toBeDeletedFiles.Add($fileName)
                    }
                    # Download file.
                    else 
                    {
                        try
                        {
                            Write-Tracing -Level Informational -Message "Downloading $fileName..."
                            $filePath = Get-TFSVersionFile -UrlConnectionInformation $UrlConnectionInformation `
                                                           -VersionControlPath $file.path `
                                                           -LocalFolder $LocalFolder
                            $downloadedFiles.Add($filePath)
                            Write-Tracing -Level Succeeded -Message "File '$fileName' downloaded successfully."
                        }
                        catch
                        {
                            Write-Tracing -Level ErrorLevel -Message "Failed to download file '$fileName'. Exception: $($_.Exception)"
                            Add-FileToSkip -FileName $fileName -Reason FailedToDownloadFile
                        }
                    }
                }
            }
        }
    }

    else #($script:sourceControlInfoObject.SourceType -eq "GitHub")
    {
        # GitHub requires TLS v1.2 or higher to create a secure channel.
        [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12

        $filesToSync = @(Get-GitHubListOfFilesToSync -UrlConnectionInformation $UrlConnectionInformation `
                                                     -FolderPath $script:sourceControlInfoObject.FolderPath `
                                                     -Branch $script:sourceControlInfoObject.Branch `
                                                     -CommitId $CommitId
                        )
        <#
        Files are blobs, and folders are trees.

        "entries": [
            {
                "name": "NestedFolder",
                "type": "tree"
            },
            {
                "name": "RB1.ps1",
                "type": "blob"
            }...
            ]
        #>
        foreach ($file in $filesToSync)
        {
            if ($file.type -eq "blob")
            {
                if (ShouldProcessFile -FileName $file.name)
                {       
                    # If the file's change is 'delete' add to delete list.
                    if ((Has-Property -Object $file -PropertyName changeIsDelete) -and $file.changeIsDelete)
                    {
                        $toBeDeletedFiles.Add($file.name)
                    }
                    # Else download file.
                    else 
                    {
                        $response = Invoke-GitHubGetRestMethod -AccountName $UrlConnectionInformation.AccountName `
                                                               -RepoName $UrlConnectionInformation.RepoName `
                                                               -Branch $script:sourceControlInfoObject.Branch `
                                                               -FolderPath $script:sourceControlInfoObject.FolderPath `
                                                               -FileName $file.name
                                                               
                        if ($response)
                        {
                            try
                            {
                                $localFilePath = Join-Path $LocalFolder $file.Name

                                $content = [Convert]::FromBase64String($response.content)
                                [IO.File]::WriteAllBytes($localFilePath, $content)

                                $downloadedFiles.Add($localFilePath)
                                Write-Tracing -Level Succeeded -Message "File '$($file.Name)' downloaded successfully."
                            }
                            catch
                            {
                                # We could not write the file content to the file.
                                Add-FileToSkip -FileName $file.Name -Reason FailedToDownloadFile
                            }
                        }
                        else
                        {
                            # File is over 1MB, so skip it.
                            Add-FileToSkip -FileName $file.name -Reason FileSizeExceedsLimit
                        }
                    }
                }
            }
        }
    }
    return @{
        downloaded = $downloadedFiles
        toDelete = $toBeDeletedFiles
    } 
}

# This function takes a file name.
# If the name is valid, and the file is a supported runbook, return true.
# Otherwise, return false, and add the file name to the list of files to skip.
# 
function ShouldProcessFile
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $FileName
    )

    $fileExtension = [System.IO.Path]::GetExtension($FileName)

    if ($script:supportedRunbookTypes -contains $fileExtension)
    {
        if (Is-ValidRunbookName -FileName $FileName)
        {
            return $true
        }
        else
        {
            Add-FileToSkip -FileName $FileName -Reason InvalidRunbookName
        }
    }
    else
    {
        Add-FileToSkip -FileName $FileName -Reason FileIsNotARunbook
    }

    # return false by default.
    return $false
}

# This function adds the given file to the global skippedFiles dictionary.
# The dictionary is organized by skip reason.
#
function Add-FileToSkip
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $FileName,

        [Parameter(Mandatory=$true)]
        [ValidateSet("FileSizeExceedsLimit", "FileIsNotARunbook", "FailedToDownloadFile",
                     "FailedToImportRunbook", "InvalidRunbookName", "FailedToDelete",
                     "NotFoundInAutomationAccount")]
        [ValidateNotNullOrEmpty()]
        [String]
        $Reason
    )

    if ($script:skippedFiles.ContainsKey($Reason))
    {
        $list = $script:skippedFiles[$Reason]
    }
    else
    {
        $list = New-Object System.Collections.Generic.List[System.Object]
    }

    # Add the file to skip to the list 
    $list.Add($FileName)
    
    $script:skippedFiles[$Reason] = $list
}

# Extract the AccountName, ProjectName, RepoName from the given url.
# Please note that VsoTfvc only has an AccountName and ProjectName.
# 
function Get-UrlConnectionInformation
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $RepoUrl,

        [Parameter(Mandatory=$true)]
        [ValidateSet("VsoGit", "VsoTfvc", "GitHub", IgnoreCase = $true)]
        [String]
        $SourceType
    )

    $uri = [Uri]::new($RepoUrl)
    $schemeAndHost = [System.Uri]::new("https://" + $uri.Host)

    # These are the building blocks for the API calls.
    $accountName = $null
    $projectName = $null
    $repoName    = $null

    # Use Uri templates to extract ProjectName, RepoName, and accountName (if any).
    $possibleRepoUrlFormats_VSTS = @{
        "VsoGit" = @(
            [System.UriTemplate]::new("_git/{repoName}"),
            [System.UriTemplate]::new("DefaultCollection/_git/{repoName}"),
            [System.UriTemplate]::new("DefaultCollection/{projectName}/_git/{repoName}"),
            [System.UriTemplate]::new("{projectName}/_git/{repoName}"),
            [System.UriTemplate]::new("{projectName}/{team}/_git/{repoName}")
        )

        "VsoTfvc" = @(
            [System.UriTemplate]::new("{projectName}/_versionControl"),
            [System.UriTemplate]::new("DefaultCollection/{projectName}/_versionControl"),
            [System.UriTemplate]::new("DefaultCollection/{projectName}/{team}/_versionControl")
        )
    }

    $possibleRepoUrlFormats_Azure_Devops = @{
        "VsoGit" = @(
            [System.UriTemplate]::new("{accountName}/{projectName}/_git/{repoName}"),
            [System.UriTemplate]::new("{accountName}/_git/{repoName}")
        )

        "VsoTfvc" = @(
            [System.UriTemplate]::new("{accountName}/{projectName}/_versionControl"),
            [System.UriTemplate]::new("{accountName}/{projectName}/{team}/_versionControl")
        )
    }

    $possibleRepoUrlFormats_Github = @(
        [System.UriTemplate]::new("{accountName}/{repoName}.git")
    );

    $azureRepos = $false
    if ($SourceType -eq "Github")
    {
        if (!($uri.Host -match "^github.com$"))
        {
            $errorMessage = "RepoUrl is not valid for SourceControl type $SourceType"
            Write-ErrorRecord -ErrorId "InvalidGithubRepoUrlFormat"
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
    }
    else
    {
        if ($uri.Host -match "^\S+.visualstudio.com$")
        {
            $azureRepos = $false
        }
        elseif ($uri.Host -match "dev.azure.com$")
        {
            $azureRepos = $true
        }
        else
        {
            $errorMessage = "RepoUrl is not valid for SourceControl type $SourceType"
            Write-ErrorRecord -ErrorId "InvalidAzureDevopsRepoUrlFormat" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
    }

    # Load assembly for UriTemplate.
    [System.Reflection.Assembly]::LoadWithPartialName("System.ServiceModel") | Out-Null

    $templates = $null;

    if ($SourceType -eq "Github")
    {
        $templates = $possibleRepoUrlFormats_Github;
    }
    else
    {
        if ($azureRepos)
        {
            $templates = $possibleRepoUrlFormats_Azure_Devops[$sourceType];
        }
        else
        {
            $templates = $possibleRepoUrlFormats_VSTS[$sourceType];
        }
    }

    #
    # Try and match the user-provided url to any of the accepted url formats in the template array.
    #
    $matchFound = $false
    foreach ($template in $templates)
    {
        $results = $template.Match($schemeAndHost, $uri)
        
        if ($results)
        {
            $matchFound  = $true
            $repoName    = $results.BoundVariables["repoName"]
            $projectName = $results.BoundVariables["projectName"]
            $accountName = $results.BoundVariables["accountName"]
            break
        }
    }

    if (!$matchFound)
    {
        Write-ErrorRecord -ErrorId "SourceControl repoUrl is invalid format for type $SourceType." `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($errorMessage))
    }

    # For VSO (repo format prior to AzureDevops), host is the accountName, for example : https://abc.visualstudio.com/
    if ([string]::IsNullOrEmpty($accountName))
    {
        $accountName = $uri.Host.Replace(".visualstudio.com", "");
    }

    if ($SourceType -eq "VsoGit")
    {
        if ((-not $projectName) -or ($projectName -eq "DefaultCollection"))
        {
            # The Uri is of the form https://[youraccount].visualstudio.com/_git/[gitRepoName], 
            # or of the form         https://[youraccount].visualstudio.com/DefaultCollection/_git/[gitRepoName]
            # so, the ProjectName is the same as the RepoName for the API calls.
            $projectName = $repoName
        }
    }
    else # $sourceType -eq "GitHub"
    {
        if (-not $accountName)
        {
            $errorMessage = "Unable to get GitHub account name from '$RepoUrl'."
            Write-ErrorRecord -ErrorId "UnableToGetGitHubAccountNameFromUrl" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
    }    

    $UrlConnectionInformation = @{
        "AccountName" = $accountName
        "RepoName" = $repoName
        "ProjectName" = $projectName
        "OriginalUrl" = $repoUrl
    }

    return $UrlConnectionInformation
}

function Get-RunbookType
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $FilePath
    )

    $type = $null
    if ($FilePath -match ".ps1")
    {        
        $ast = [System.Management.Automation.Language.Parser]::ParseFile($FilePath, [ref]$null, [ref]$null)
        if ($ast.EndBlock.Extent.Text -match "^workflow")
        {
            $type = "PowerShellWorkflow"
        }
        elseif ($ast.EndBlock.Extent.Text -match "^configuration")
        {
            $type = "Configuration"
        }
        else
        {
            # PowerShell refers to PS scripts. Set default as powershell 5
            # if the first line states with powershell7, use it as a powershell 7
            if ($ast -match "^#powershell7") {
                $type = "PowerShell7"
            } else {
                $type = "PowerShell"
            }
        }
    }

    elseif ($FilePath -match ".graphrunbook")
    {
        $type = "Graph"
    }

    elseif ($FilePath -match ".py")
    {
        $type = "Python2"
    }

    return $type
}

function Has-Property
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Object,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $PropertyName
    )

    if (Get-Member -InputObject $Object -Name $PropertyName)
    {
        return $true
    }

    return $false
}

function Is-ValidSize
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Path
    )

    $isValidSize = $false

    if (Test-Path $Path)
    {
        $fileSize = (Get-Item $Path).Length
        $isValidSize = ($fileSize -le $script:maxFileSize)
    }

    return $isValidSize
}

# This function takes a message and concatenates the error message from $ExceptionThrown 
# to provide additional context.
function New-ErrorMessage
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNull()]
        $ExceptionThrown,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Message
    )

    $additionalInfo = $script:newLine + "Exception: "

    if (Has-Property -Object $ExceptionThrown -PropertyName Exception)
    {
        if (Has-Property -Object $ExceptionThrown.Exception -PropertyName Message)
        {
            $additionalInfo += $($ExceptionThrown.Exception.Message)
        }
        else
        {
            $additionalInfo += $($ExceptionThrown.Exception)
        }
    }
    else
    {
        $additionalInfo += $ExceptionThrown.ToString()
    }

    $Message += $additionalInfo

    return $Message
}

function Write-ErrorRecord
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $ErrorId,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $ErrorCategory,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Exception,

        [Parameter(Mandatory=$false)]
        [ValidateSet("TerminatingError", "NonTerminatingError", IgnoreCase = $true)]
        [String]
        $Type = "TerminatingError"
    )
    $script:ReasonToNotStartSyncJob = $Exception.Message
    $errorRecord = [System.Management.Automation.ErrorRecord]::New($Exception, $ErrorId, $ErrorCategory, $null)
    Write-Error -ErrorRecord $errorRecord
    if ($Type -eq "TerminatingError")
    {
        throw $errorRecord
    }
}

# This function normalized the path without validating if it exists.
#
function Normalize-Path
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Path,

        [Parameter(Mandatory=$true)]
        [ValidateSet("VsoGit", "VsoTfvc", "GitHub")]
        [String]
        $SourceType
    )

    $normalizedPath = $Path

    if ($normalizedPath.Contains("\"))
    {
        $normalizedPath = $normalizedPath.Replace("\", "/")
    }

    if ($SourceType -eq "GitHub")
    {
        # For GitHub we need to trim the forward slash 
        # at the beginning of the folder path
        $normalizedPath = $normalizedPath.TrimStart('/')

        # If the folder path ends with a forward slash, 
        # remove that as well
        $normalizedPath = $normalizedPath.TrimEnd('/')
    }

    return $normalizedPath
}

function Write-RunbookListSummary
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [AllowEmptyCollection()]
        [System.Collections.Generic.List[System.Object]]
        $RunbookList,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]
        $Action
    )

    $fileMessage = if ($RunbookList.Count -eq 1) { "{0} file ${Action}:" } else {"{0} files ${Action}:"}

    foreach ($runbookName in $RunbookList)
    {
        $fileMessage += $script:newLine + " - " + $runbookName
    }

    $mainMessage = $script:newLine + ($fileMessage -f $RunbookList.Count)

    return $mainMessage
}

function Write-Summary
{
    Param
    (
        [Parameter(Mandatory=$true)]
        $SkippedFiles,

        [Parameter(Mandatory=$true)]
        [AllowEmptyCollection()]
        [System.Collections.Generic.List[System.Object]]
        $ImportedRunbooks,

        [Parameter(Mandatory=$false)]
        [AllowEmptyCollection()]
        [System.Collections.Generic.List[System.Object]]
        $DeletedRunbooks
    )

    # Throw a terminating error if there were errors while syncing the user runbooks.
    # This can be determined by taking a look SkippedFiles.
    $throwFinalTerminatinError = $false

    $message = $script:newLine + "Source Control Sync Summary:"
    Write-Tracing -Level Informational -Message $message -DisplayMessageToUser

    if ($ImportedRunbooks.Count -gt 0)
    {
        $mainMessage = Write-RunbookListSummary -RunbookList $ImportedRunbooks -Action "synced"
        Write-Tracing -Level Informational -Message $mainMessage -DisplayMessageToUser
    }

    if ($DeletedRunbooks.Count -gt 0)
    {
        $mainMessage = Write-RunbookListSummary -RunbookList $DeletedRunbooks -Action "deleted"
        Write-Tracing -Level Informational -Message $mainMessage -DisplayMessageToUser
    }
       
    if ($SkippedFiles.Count -gt 0)
    {
        foreach ($reason in $SkippedFiles.Keys)
        {
            $list = $SkippedFiles[$reason]

            if ($list.Count -gt 0)
            {
                $message = $script:newLine + (Get-SkipReason -Reason $reason)

                foreach ($item in $list)
                {
                    $message += $script:newLine + " - " + $item
                }

                Write-Tracing -Level Informational -Message $message -DisplayMessageToUser

                # If a runbook was not synced, let the user know by writing an error per each category.
                if ($reason -ne "FileIsNotARunbook")
                {
                    $throwFinalTerminatinError = $true
                    $errorMessage = "Failed to sync. " + $message
                    Write-ErrorRecord -ErrorId $reason `
                                      -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                      -Exception ([System.InvalidOperationException]::New($errorMessage)) `
                                      -Type NonTerminatingError
                }
            }
        }
    }

    $message = $script:newLine
    $message += $script:newLine
    $message += "========================================================================================================"
    Write-Tracing -Level Informational -Message $message -DisplayMessageToUser

    if ($throwFinalTerminatinError)
    {
        $errorMessage = "There were errors while syncing the user runbooks. Please see error streams for more information."
        Write-ErrorRecord -ErrorId "ErrorsOccurredWhileSyncingTheUserRunbooks" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($errorMessage))
    }
}

function Get-SkipReason
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Reason
    )

    switch ($Reason)
    {
        "FailedToDownloadFile"
        {
            return "Failed to download file:"
        }

        "FailedToImportRunbook"
        {
            return "Failed to import runbook:"
        }

        "FileIsNotARunbook"
        {
            return "File is not a runbook:"
        }

        "FileSizeExceedsLimit"
        {
            return "File size exceeds $($script:maxFileSize/1Mb)Mb:"
        }

        "InvalidRunbookName"
        {
            return "Invalid runbook name:"
        }

        "FailedToDelete"
        {
            return "Failed to delete:"
        }

        "NotFoundInAutomationAccount"
        {
            return "Not found in Automation Account:"
        }
    }
}

function Write-WelcomeMessage
{
    $message = "========================================================================================================"
    $message += $script:newLine
    $message += $script:newLine  + "Azure Automation Source Control."
    $message += $script:newLine  + "Supported runbooks to sync: PowerShell Workflow, PowerShell Scripts, DSC Configurations, Graphical, and Python 2."
    Write-Tracing -Level Informational -Message $message -DisplayMessageToUser
}

function Set-ResourceManagerEndpoint
{
    $azureEnvironment = $script:AzureEnvironmentToResourceManagerURL[$AzureEnvironmentName]
    # Add $azureEnvironment to the automationAccountInfo object.
    $script:automationAccountInfo["AzureEnvironment"] = $azureEnvironment
    $script:apiEndpoint = $azureEnvironment

    if (-not $script:apiEndpoint)
    {
        $exception = [System.InvalidOperationException]::New("Unable to get 'ResourceManagerUrl' from AzureRmEnvironment")
        Write-ErrorRecord -ErrorId "UnableToRetrieveResourceManagerUrl" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception $exception
    }
}

# A valid runbook name must contain only letters, numbers, underscores, and dashes.
# The name must begin with a letter.  The name must be less than 64 characters.
#
function Is-ValidRunbookName
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $FileName
    )

    # Remove the file extension from the file name.
    $FileName = [System.IO.Path]::GetFileNameWithoutExtension($FileName)

    $regex = "^[a-zA-Z]{1}[a-zA-Z0-9_-]{0,63}$"
    return ($FileName -match $regex)
}

# Evaluate the response based on the following:
# 1) ValidateLoginOnly - Validate that $Response is the output of a successful call 
#                        (that the user security token is correct). If this is the case, 
#                        just return the $Response.
# 2) ValidatePropertyExists - In addition to ValidateLoginOnly, it also ensures that the
#                             given property exists. If this is the case, it is valid respose, 
#                             so  just return the $Response.
# 3) ValidateAndReturnProperty - In addition to ValidatePropertyExists, return $Response.PropertyName.
# 
# Login failure scenarios: - If we received a string response, which is the content of the signin page. 
#                            This implies invalid credentials.
#                          - If we do not receive a response. This implies invalid credentials as well.
# 
function VerifyResponse
{
    Param
    (
        [Parameter(Mandatory=$false)]
        $Response,

        [Parameter(Mandatory=$false)]
        [ValidateNotNullOrEmpty()]
        [String]
        $PropertyName,

        [Parameter(Mandatory=$true)]
        [ValidateSet("ValidateLoginOnly", "ValidatePropertyExists", "ValidateAndReturnProperty")]
        [String]
        $ValidationType
    )

    $throwException = $false

    if (-not $Response)
    {
        $throwException = $true
    }
    elseif (($Response.GetType().Name -eq "String") -and ($Response -match "Sign In"))
    {
        $throwException = $true
    }

    if ($throwException)
    {
        $errorMessage = "Invalid security token."
        Write-ErrorRecord -ErrorId "Unauthorized" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::AuthenticationError) `
                          -Exception ([System.InvalidOperationException]::New($errorMessage))
    }

    if ($ValidationType -eq "ValidateLoginOnly")
    {
        return $Response
    }
    elseif (($ValidationType -eq "ValidatePropertyExists") -or 
            ($ValidationType -eq "ValidateAndReturnProperty"))
    {
        if (Has-Property -Object $Response -PropertyName $PropertyName)
        {
            if ($ValidationType -eq "ValidateAndReturnProperty")
            {
                return $Response.$PropertyName
            }

            return $Response
        }
        else
        {
            $errorMessage = "The response does not contain the expected '$PropertyName' property."
            Write-ErrorRecord -ErrorId "InvalidResponse" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }
    }
}

function Is-ValidCommitId
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $CommitId
    )

    # GitHub and VsoGit commit ids must only contain hexadecimal characters that specify a 160-bit SHA-1 hash.
    # VsoTfvc change set id must only contain decimal characters.
    $regex = "^[0-9a-zA-Z]{1,128}$"

    return ($CommitId -match $regex)
}
#endregion
function Get-AccessToken
{
    $token = $null
    if($script:Automation_Sourcecontrol_Auth_Mechanism -eq "User-Assigned Managed Identity")
    {
        $ClientId = Get-AutomationVariable -Name "AUTOMATION_SC_USER_ASSIGNED_IDENTITY_ID"
        $Response = Get-ManagedIdentityAccessToken -ClientId $ClientId
        $token = $Response.access_token
    }
    elseif($script:Automation_Sourcecontrol_Auth_Mechanism -eq "System-Assigned Managed Identity")
    {
        $Response = Get-ManagedIdentityAccessToken
        $token = $Response.access_token
    }
    elseif($script:Automation_Sourcecontrol_Auth_Mechanism -eq "RunAsAccount")
    {
        $servicePrincipalConnection = Get-AutomationConnection -Name "AzureRunAsConnection" 
        $TenantId = $servicePrincipalConnection.TenantId
        $ApplicationId = $servicePrincipalConnection.ApplicationId

        $Certificate = Get-AutomationCertificate -Name "AzureRunAsCertificate"

        # Create base64 hash of certificate
        $CertificateBase64Hash = [System.Convert]::ToBase64String($Certificate.GetCertHash())

        # Create JWT timestamp for expiration
        $StartDate = (Get-Date "1970-01-01T00:00:00Z" ).ToUniversalTime()
        $JWTExpirationTimeSpan = (New-TimeSpan -Start $StartDate -End (Get-Date).ToUniversalTime().AddMinutes(10)).TotalSeconds
        $JWTExpiration = [math]::Round($JWTExpirationTimeSpan,0)

        # Create JWT validity start timestamp
        $NotBeforeExpirationTimeSpan = (New-TimeSpan -Start $StartDate -End ((Get-Date).ToUniversalTime())).TotalSeconds
        $NotBefore = [math]::Round($NotBeforeExpirationTimeSpan,0)

        # Create JWT header
        $JWTHeader = @{
            alg = "RS256"
            typ = "JWT"
            # Use the CertificateBase64Hash and replace/strip to match web encoding of base64
            x5t = $CertificateBase64Hash -replace '\+','-' -replace '/','_' -replace '='
        }

        $activeDirectoryEndPoint = $script:AzureEnvironmentToActiveDirectory[$AzureEnvironmentName]

        # Create JWT payload
        $JWTPayLoad = @{
            # What endpoint is allowed to use this JWT
            aud = "$activeDirectoryEndPoint/$TenantId/oauth2/token"

            # Expiration timestamp
            exp = $JWTExpiration

            # Issuer = your application
            iss = $ApplicationId

            # JWT ID: random guid
            jti = [guid]::NewGuid()

            # Not to be used before
            nbf = $NotBefore

            # JWT Subject
            sub = $ApplicationId
        }

        # Convert header and payload to base64
        $JWTHeaderToByte = [System.Text.Encoding]::UTF8.GetBytes(($JWTHeader | ConvertTo-Json))
        $EncodedHeader = [System.Convert]::ToBase64String($JWTHeaderToByte)

        $JWTPayLoadToByte =  [System.Text.Encoding]::UTF8.GetBytes(($JWTPayload | ConvertTo-Json))
        $EncodedPayload = [System.Convert]::ToBase64String($JWTPayLoadToByte)

        # Join header and Payload with "." to create a valid (unsigned) JWT
        $JWT = $EncodedHeader + "." + $EncodedPayload

        # Get the private key object of your certificate
        $PrivateKey = New-Object -TypeName 'System.Security.Cryptography.RSACryptoServiceProvider'
        $PrivateKey.FromXmlString($Certificate.PrivateKey.ToXmlString($true))

        # Define RSA signature and hashing algorithm
        $RSAPadding = [Security.Cryptography.RSASignaturePadding]::Pkcs1
        $HashAlgorithm = [Security.Cryptography.HashAlgorithmName]::SHA256

        # Create a signature of the JWT
        $Signature = [Convert]::ToBase64String(
            $PrivateKey.SignData([System.Text.Encoding]::UTF8.GetBytes($JWT),$HashAlgorithm,$RSAPadding)
        ) -replace '\+','-' -replace '/','_' -replace '='

        # Join the signature to the JWT with "."
        $JWT = $JWT + "." + $Signature

        $resourceManagerEndPoint = $script:AzureEnvironmentToResourceManagerURL[$AzureEnvironmentName]
        $Scope = "$resourceManagerEndPoint/.default"
        # Create a hash with body parameters
        $Body = @{
            client_id = $ApplicationId
            client_assertion = $JWT
            client_assertion_type = "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"
            scope = $Scope
            grant_type = "client_credentials"

        }

        $Url = "$activeDirectoryEndPoint/$TenantId/oauth2/v2.0/token"

        # Use the self-generated JWT as Authorization
        $Header = @{
            Authorization = "Bearer $JWT"
        }
        try
        {
            $response = Invoke-WebRequest -Method Post -Uri $Url -Headers $Header -Body $Body -ContentType 'application/x-www-form-urlencoded' -ErrorAction Stop -UseBasicParsing
            $statusCode = $response.StatusCode
            if($statusCode -eq 200)
            {
                $jsonObj = $response.Content | ConvertFrom-Json
                $token = $jsonObj.access_token
            }
            else
            {
            $message = "Failed to get access token (statuscode: $statusCode)."
            Write-ErrorRecord  -ErrorId "UnableToRetrieveAcessToken" `
                                -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                -Exception ([System.InvalidOperationException]::New($message))
            }
        }
        catch [System.Net.WebException]
        {
            $statusCode = $_.Exception.Response.StatusCode.value__
            $statusDescription = $_.Exception.Response.StatusDescription
            $errorMessage = $null
            If ($statusDescription)
            {
                $errorMessage = "Failed to get access token with $statusDescription($statusCode)." + $script:newLine
            }
            else
            {
                $errorMessage = "Failed to get access token with status code: $statusCode." + $script:newLine
            }
            $exception = [System.InvalidOperationException]::New($errorMessage)
            Write-ErrorRecord -ErrorId "UnableToRetrieveAcessToken" `
                                -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                -Exception $exception
        }
    }
    return $token
}

# Path to temporary folder to download the user runbooks, which will be imported to their Azure Automation account.
$psFolderPath = $null
try
{
    Write-Tracing -Level Informational -Message "Sync Source Control script started."
    Write-Tracing -Level Informational -Message "Getting AzureRunAsConnection information."
    Write-WelcomeMessage

    if (($Mode -eq "Production") -or ($Mode -eq "AzurePortalDevelopment"))
    {
        # Setting AzureEnvironment
        Write-Tracing -Level Informational -message "Setting AzureEnvironment." -DisplayMessageToUser

        # Set the API endpoint (DogFood or Production).
        Set-ResourceManagerEndpoint 

        Get-AuthenticationMechanismForRunbook
    }

    # Get and set the source control information.
    if (-not $script:sourceControlInfoObject)
    {
        # Go through ARM to retrieve the source control information.
        Write-Tracing -Level Informational -message "Getting the source control information"

        # Set $script:sourceControlInfoObject. 
        Set-AzureAutomationSourceControl
    }

    $repoUrl        = $script:sourceControlInfoObject.RepoUrl
    $branch         = $script:sourceControlInfoObject.Branch
    $folderPath     = $script:sourceControlInfoObject.FolderPath
    $securityToken  = $script:sourceControlInfoObject.SecurityToken
    $sourceType     = $script:sourceControlInfoObject.SourceType
    $publishRunbook = $script:sourceControlInfoObject.PublishRunbook
    $autoSync       = $script:sourceControlInfoObject.AutoSync

    $message = "Source control information for syncing:"
    Write-Tracing -Level Informational -Message $message -DisplayMessageToUser

    if (($sourceType -eq "VsoGit") -or ($sourceType -eq "GitHub"))
    {
        $message = "[RepoUrl = $repoUrl] [Branch  = $branch] [FolderPath = $folderPath]"
    }
    else # "VsoTfvc"
    {
        $message = "[Url = $repoUrl] [FolderPath = $folderPath]"
    }
    Write-Tracing -Level Informational -Message $message -DisplayMessageToUser

    # Verify the url.
    Write-Tracing -Level Informational -Message ("Verifying url: $repoUrl") -DisplayMessageToUser
    $UrlConnectionInformation = Get-UrlConnectionInformation -RepoUrl $repoUrl -SourceType $sourceType
    $accountName = $UrlConnectionInformation.AccountName
    $repoName = $UrlConnectionInformation.RepoName
    $projectName = $UrlConnectionInformation.ProjectName
    $message = "UrlConnectionInformation: [AccountName = $accountName] [RepoName = $repoName] [ProjectName = $projectName]"
    Write-Tracing -Level Informational -Message $message  

    # Create the temporary folder.
    $psFolderPath = Join-Path $env:temp (new-guid).Guid
    New-Item -Path $psFolderPath -ItemType Directory -Force | Out-Null
    Write-Tracing -Level Informational -Message "Creating temporary directory $psFolderPath"
    
    # Let the user know that we are connecting to GitHub or VSTS.
    $message = if ($sourceType -eq "GitHub") { "Connecting to GitHub..." } else { "Connecting to VSTS..." }
    Write-Tracing -Level Informational -Message ($message) -DisplayMessageToUser

    # Download the user runbooks from GitHub or VSTS.
    Write-Tracing -Level Informational -Message "Syncing files..."

    # If we are syncing to a given commitId, make sure it has the correct format.
    if ($CommitId)
    {
        if (-not (Is-ValidCommitId -CommitId $CommitId))
        {
            $errorMessage = "Commit id '{0}' must only contain hex or base64 characters." -f $CommitId
            Write-ErrorRecord -ErrorId "InvalidCommitIdFormat" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage))
        }

        # Add the CommitId to the sourceControlInfoObject if available.
        $script:sourceControlInfoObject["CommitId"] = $CommitId

        # Let the user know that we are syncing to a commit id or change set id.
        $message = "Syncing to "
        if ($sourceType -eq "VsoTfvc")
        {
            $message += "change set id: '$CommitId'."
        }
        else
        {
            $message += "commit id: '$CommitId'."
        }
        Write-Tracing -Level Informational -Message $message -DisplayMessageToUser
    }

    # Get list of files to sync.
    $filesToSync = Get-SourceControlFile -UrlConnectionInformation $UrlConnectionInformation `
                                         -LocalFolder $psFolderPath `
                                         -CommitId $CommitId

    # Import files downloaded to temporary folder to Azure.
    $importedRunbooks = New-Object System.Collections.Generic.List[System.Object]

    # As per bug: 4924147 : Log and Trace settings should persist over syncs.
    # Get All runbooks and dsc configurations and use current trace and log settings.

    # Get all runbooks for this automation account

    # Request uri.
    $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/runbooks`?api-version=$script:apiVersion"
    # headers
    $AcessToken = Get-AccessToken
    $headers = @{
      "Authorization" = "Bearer {0}" -f $AcessToken
      "Content-Type" = "application/json"
    }

    $runbookNameToRunbookObjectMapping = @{}
    try{
        $response = Invoke-WebRequest -Method Get -Uri $uri -Headers $headers -ErrorAction Stop -UseBasicParsing
        $statusCode = $response.StatusCode
        if($statusCode -eq 200)
        {
            $jsonResponse = $response.Content | ConvertFrom-Json
            $runbooks = $jsonResponse.value
            
            foreach ($runbook in $runbooks)
            {
                $runbookNameToRunbookObjectMapping.Add($runbook.name.ToLower(), $runbook)
            }
        }
        else
        {
            $message = "WebRequest failed to get all runbooks for this automation account with status code: $statusCode"
            Write-ErrorRecord  -ErrorId "FailedToGetRunbooks" `
                               -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                               -Exception ([System.InvalidOperationException]::New($message))
        }
    }
    catch [System.Net.WebException]
    {
        $statusCode = $_.Exception.Response.StatusCode.value__
        $statusDescription = $_.Exception.Response.StatusDescription
        $message = $null
        If ($statusDescription)
        {
            $message = "WebRequest failed to get all runbooks for automation account with $statusDescription($statusCode)"
        }
        else
        {
            $message = "WebRequest failed to get all runbooks for automation account with status code: $statusCode"
        }
        Write-ErrorRecord -ErrorId "FailedToGetRunbooks" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($message))
    }

    # Get all dsc configurations for this automation account
    # Request uri.
    $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/configurations`?api-version=$script:apiVersion"
    # headers
    $AcessToken = Get-AccessToken
    $headers = @{
      "Authorization" = "Bearer {0}" -f $AcessToken
      "Content-Type" = "application/json"
    }

    $dscConfigNameToDscConfigObjectMapping = @{}
    try{
        $response = Invoke-WebRequest -Method Get -Uri $uri -Headers $headers -ErrorAction Stop -UseBasicParsing
        $statusCode = $response.StatusCode
        if($statusCode -eq 200)
        {
            $jsonResponse = $response.Content | ConvertFrom-Json
            $dscConfigurations = $jsonResponse.value
            foreach ($dscConfig in $dscConfigurations)
            {
                $dscConfigNameToDscConfigObjectMapping.Add($dscConfig.name.ToLower(), $dscConfig)
            }
        }
        else
        {
            $message = "WebRequest failed to get all dsc configurations for automation account with status code: $statusCode"
            Write-ErrorRecord -ErrorId "FailedToGetDscConfigurations" `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($message))
        }
    }
    catch [System.Net.WebException]
    {
        $statusCode = $_.Exception.Response.StatusCode.value__
        $statusDescription = $_.Exception.Response.StatusDescription
        $message = $null
        If ($statusDescription)
        {
            $message = "WebRequest failed to get all dsc configurations for automation account with $statusDescription($statusCode)"
        }
        else
        {
            $message = "WebRequest failed to get all dsc configurations for automation account with status code: $statusCode"
        }
        Write-ErrorRecord -ErrorId "FailedToGetDscConfigurations" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($message))
    }
    # Set Automation account location
    $accountLocation = $null
    try
    {
        $AcessToken = Get-AccessToken
        $requestHeader = @{
          "Authorization" = "Bearer {0}" -f $AcessToken
          "Content-Type" = "application/json"
        }
        $uriAcct = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName`?api-version=$script:apiVersion"
        $response = Invoke-WebRequest -Method Get -Headers $requestHeader -Uri $uriAcct -ErrorAction Stop -UseBasicParsing
        $accountInfo =  $response.Content | ConvertFrom-Json
        $accountLocation = $accountInfo.location
    }
    catch [System.Net.WebException]
    {
        $statusCode = $_.Exception.Response.StatusCode.value__ 
        $statusDescription = $_.Exception.Response.StatusDescription
        $message = $null
        If ($statusDescription)
        {
            $message = "WebRequest failed to get location for automation account with $statusDescription($statusCode)"
        }
        else
        {
            $message = "WebRequest failed to get location for automation account with status code: $statusCode"
        }
        Write-ErrorRecord -ErrorId "FailedToGetAutomationAccountLocation" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception ([System.InvalidOperationException]::New($message))
    }

    
    $filelist = @{}
    foreach ($fileobj in $filesToSync) {
        if ($fileobj.gettype().name -match 'Hashtable') {
             $filelist = $fileobj
        }
    }

    foreach ($filePath in $filelist.downloaded)
    {
        $fileName = Split-Path $filePath -Leaf

        if (-not (Is-ValidSize -Path $filePath))
        {
            Write-Tracing -Level Informational -Message ("Runbook '{0}' exceeds size limit." -f $fileName)
            Add-FileToSkip -FileName $fileName -Reason FileSizeExceedsLimit
            continue
        }

        Write-Tracing -Level Informational -Message "Getting the runbook type."
        $runbookType = Get-RunbookType -FilePath $filePath

        Write-Tracing -Level Informational -Message "RunbookType: $runbookType"

        try
        {
            $successfulImport = $false
            $runbookNameWithoutExtension = [System.IO.Path]::GetFileNameWithoutExtension($filePath)
            $fileNameWithoutExtension = ([System.IO.Path]::GetFileNameWithoutExtension($filePath)).toLower()

            if (($Mode -eq "Production") -or ($Mode -eq "AzurePortalDevelopment"))
            {
                if ($runbookType -eq "Configuration")
                {
                    Write-Tracing -Level Informational -Message "Updating dsc configuration via REST API."

                    $existingDscConfig = $dscConfigNameToDscConfigObjectMapping[$fileNameWithoutExtension]
                    $logVerbose = $false
                    $description = ""
                    $configurationTags = @{}
                    if($existingDscConfig)
                    {
                        $properties = $existingDscConfig.properties
                        if("logverbose" -in $properties)
                        {
                            $logVerbose = $properties.logverbose
                        }
                        if("description" -in $properties)
                        {
                            $description = $properties.description
                        }
                        $accountLocation = $existingDscConfig.location
                        $configurationTags = $existingDscConfig.tags
                    }
                    $AcessToken = Get-AccessToken

                    # Request uri.
                    $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/configurations/$runbookNameWithoutExtension`?api-version=$script:apiVersion"
                    $requestHeader = @{
                        "Authorization" = "Bearer {0}" -f $AcessToken
                        "Content-Type" = "application/json"
                    }

                    $content = [IO.File]::ReadAllText($filePath)

                    try{
                        
                        $Body = @{
                            properties = @{
                                source = @{
                                    type = 'embeddedContent'
                                    value = $content
                                }
                                description = $description
                                logVerbose = $logVerbose
                            }
                            name = $runbookNameWithoutExtension
                            location = $accountLocation
                            tags = $configurationTags
                        } | ConvertTo-Json
                        
                        $result =  Invoke-RestMethod -Method Put -Body $Body -Headers $requestHeader -Uri $uri -ErrorAction Stop -UseBasicParsing

                        Write-Tracing -Level Succeeded -Message "Configuration '$fileName' successfully imported."
                        $successfulImport = $true
                    }
                    catch [System.Net.WebException]
                    {
                       $statusCode = $_.Exception.Response.StatusCode.value__
                       $statusDescription = $_.Exception.Response.StatusDescription
                       $message = $null
                       If ($statusDescription)
                       {
                           $message = "Failed to create or update dsc configuration for automation account with $statusDescription($statusCode)"
                       }
                       else
                       {
                           $message = "Failed to create or update dsc configuration for automation account with status code: $statusCode"
                       }
                       Write-ErrorRecord -ErrorId "FailedToCreateOrUpdateDscConfiguration" `
                                         -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                         -Exception ([System.InvalidOperationException]::New($message))
                    }
                }
                else
                {
                    Write-Tracing -Level Informational -Message "Updating runbook via REST API."

                    # Sync only if the runbook types are same, Otherwise existing runbook cannot be updated.
                    if($runbookNameToRunbookObjectMapping.ContainsKey($fileNameWithoutExtension) -and $runbookType.ToLower() -ne $runbookNameToRunbookObjectMapping[$fileNameWithoutExtension].properties.runbookType.ToLower())
                    {
                        $runbookTypeToSync = $runbookNameToRunbookObjectMapping[$fileNameWithoutExtension].properties.runbookType
                        throw "$runbookTypeToSync Runbook with $fileNameWithoutExtension name already exist in automation account."
                    }

                    $runbookImportSuccessful = $true
                    $AcessToken = Get-AccessToken

                    if(-not $runbookNameToRunbookObjectMapping.ContainsKey($fileNameWithoutExtension))
                    {
                        try
                            {
                                $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/runbooks/$runbookNameWithoutExtension`?api-version=$script:apiVersion"
                                $requestHeader = @{
                                    "Authorization" = "Bearer {0}" -f $AcessToken
                                    "Content-Type" = "application/json"
                                }
                                $Body = @{
                                    properties = @{
                                        runbookType = $runbookType
                                        draft = @{}
                                    }
                                    name = $runbookNameWithoutExtension
                                    location = $accountLocation
                                } | ConvertTo-Json

                                $result = Invoke-RestMethod -Method Put -Body $Body -Headers $requestHeader -Uri $uri -ErrorAction Stop -UseBasicParsing
                            }
                            catch
                            {
                               $runbookImportSuccessful = $false
                               $statusCode = $_.Exception.Response.StatusCode.value__ 
                               $statusDescription = $_.Exception.Response.StatusDescription
                               $message = $null
                               If ($statusDescription)
                               {
                                   $message = "Failed to create runbook as draft for automation account with $statusDescription($statusCode)"
                               }
                               else
                               {
                                   $message = "Failed to create runbook as draft for automation account with status code: $statusCode"
                               }
                               Write-ErrorRecord -ErrorId "FailedToCreateRunbookAsDraft" `
                                                 -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                                 -Exception ([System.InvalidOperationException]::New($message))
                            }
                    }

                    # Replace Runbook Content
                    # Request uri.
                    $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/runbooks/$fileNameWithoutExtension/draft/content`?api-version=$script:apiVersion"
                    $requestHeader = @{
                        "Authorization" = "Bearer {0}" -f $AcessToken
                        "Content-Type" = "application/octet-stream"
                    }
                    try{
                        $result = Invoke-WebRequest -Method Put -InFile $filePath -Headers $requestheader -Uri $uri -ErrorAction Stop -UseBasicParsing
                        $statusCode = $result.StatusCode
                        if ($statusCode -eq 202)
                        {
                            $location = $result.Headers["location"]
                            $retryAfter = $result.Headers["retry-after"]
                        }
                        $timeout = New-TimeSpan -Minutes 30
                        $stopwatch = [System.Diagnostics.Stopwatch]::StartNew()
                        do
                        {
                            $response = Invoke-WebRequest -Method Get -Headers $requestHeader -Uri $location -ErrorAction Stop -UseBasicParsing
                            $statusCode = $response.StatusCode
                            if($statusCode -ne 200)
                            {
                                Start-Sleep -Seconds $retryAfter
                            }
                        }
                        while($statusCode -ne 200 -and $stopwatch.elapsed -lt $timeout)
                    }
                    catch [System.Net.WebException]
                    {
                        $runbookImportSuccessful = $false
                        $statusCode = $_.Exception.Response.StatusCode.value__ 
                        $statusDescription = $_.Exception.Response.StatusDescription
                        $message = $null
                        If ($statusDescription)
                        {
                            $message = "Failed to Replace Runbook Content for automation account with $statusDescription($statusCode)"
                        }
                        else
                        {
                            $message = "Failed to Replace Runbook Content for automation account with status code: $statusCode"
                        }
                        Write-ErrorRecord -ErrorId "FailedToReplaceRunbookContent" `
                                            -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                            -Exception ([System.InvalidOperationException]::New($message))
                    }

                    # Publish Runbook
                    if($publishRunbook)
                    {
                        $requestHeader = @{
                            "Authorization" = "Bearer {0}" -f $AcessToken
                            "Content-Type" = "application/json"
                        }
                        # Request uri.
                        $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/runbooks/$fileNameWithoutExtension/draft/publish`?api-version=$script:apiVersion"
                    
                        try{
                            $result = Invoke-WebRequest -Method Post -Headers $requestheader -Uri $uri -ErrorAction Stop -UseBasicParsing
                            $statusCode = $result.StatusCode
                            if ($statusCode -eq 202)
                            {
                                $location = $result.Headers["location"]
                                $retryAfter = $result.Headers["retry-after"]
                            }
                            $timeout = New-TimeSpan -Minutes 30
                            $stopwatch = [System.Diagnostics.Stopwatch]::StartNew()
                            do
                            {       
                                $response = Invoke-WebRequest -Method Get -Headers $requestHeader -Uri $location -ErrorAction Stop -UseBasicParsing
                                $statusCode = $response.StatusCode
                                if($statusCode -ne 200)
                                {
                                    Start-Sleep -Seconds $retryAfter
                                }
                            }
                            while($statusCode -ne 200 -and $stopwatch.elapsed -lt $timeout)
                        }
                        catch [System.Net.WebException]
                        {
                            $runbookImportSuccessful = $false
                            $statusCode = $_.Exception.Response.StatusCode.value__ 
                            $statusDescription = $_.Exception.Response.StatusDescription
                            $message = $null
                            If ($statusDescription)
                            {
                                $message = "Failed to Publish Runbook for automation account with $statusDescription($statusCode)"
                            }
                            else
                            {
                                $message = "Failed to Publish Runbook for automation account with status code: $statusCode"
                            }
                            Write-ErrorRecord -ErrorId "FailedToPublishRunbook" `
                                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                              -Exception ([System.InvalidOperationException]::New($message))
                        }
                    }

                    if($runbookImportSuccessful)
                    {
                        Write-Tracing -Level Succeeded -Message "Runbook '$fileName' successfully imported."
                        $successfulImport = $true
                    }
                }
            }

            if ($successfulImport -or ($Mode -eq "LocalDevelopment"))
            {
                $importedRunbooks.Add($fileName)
            }
        }
        catch
        {
            $reason = "FailedToImportRunbook"
            $errorMessage = New-ErrorMessage -ExceptionThrown $_ -Message "Failed to import runbook '$fileName'."
            Write-ErrorRecord -ErrorId $reason `
                              -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                              -Exception ([System.InvalidOperationException]::New($errorMessage)) `
                              -Type NonTerminatingError

            Add-FileToSkip -FileName $fileName -Reason $reason
        }
    }

    # Files to be deleted from user's Azure Automation account.
    $deletedRunbooks = New-Object System.Collections.Generic.List[System.Object]

    foreach ($file in $filelist.toDelete)
    {
        $fileName = [System.IO.Path]::GetFileNameWithoutExtension($file)

        $successfulDelete, $deleteError, $runbookNotFound, $configNotFound = $false

        $isRunbook, $isDSCConfig = $null

        if (($Mode -eq "Production") -or ($Mode -eq "AzurePortalDevelopment"))
        {
            # We don't know if file is a runbook or DSC configuration, so first try to get runbook from Azure Automation account.
            try
            {
                Write-Tracing -Level Informational -Message "Try to get runbook from Azure via REST API"
                
                # Request uri.
                $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/runbooks/$fileName`?api-version=$script:apiVersion"
                # headers
                $AcessToken = Get-AccessToken
                $headers = @{
                  "Authorization" = "Bearer {0}" -f $AcessToken
                  "Content-Type" = "application/json"
                }

                $isRunbook = Invoke-WebRequest -Uri $uri -Headers $headers -Method Get -ErrorAction Stop -UseBasicParsing
                
            }
            catch [System.Net.WebException]
            {
                $runbookNotFound = $true
            }

            if ($isRunbook)
            {
                # We know file is a runbook.
                Write-Tracing -Level Informational -Message "Deleting runbook via REST API."

                try
                {
                    # Request uri.
                    $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/runbooks/$fileName`?api-version=$script:apiVersion"
                    # headers
                    $AcessToken = Get-AccessToken
                    $headers = @{
                      "Authorization" = "Bearer {0}" -f $AcessToken
                      "Content-Type" = "application/json"
                    }
                    $null = Invoke-WebRequest -Uri $uri -Headers $headers -Method Delete -ErrorAction Stop
                    
                    Write-Tracing -Level Succeeded -Message "Runbook '$file' successfully deleted."
                    $successfulDelete = $true
                }
                catch [System.Net.WebException]
                {
                    $reason = "FailedToDelete"
                    $errorMessage = New-ErrorMessage -ExceptionThrown $_ -Message "Failed to delete '$file'."
                    Write-ErrorRecord -ErrorId $reason `
                                      -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                      -Exception ([System.InvalidOperationException]::New($errorMessage)) `
                                      -Type NonTerminatingError
                }
            }
            # Try to get DSC configuration from Azure Automation account.
            else
            {
                try
                {
                    # Request uri.
                    $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/configurations/$fileName`?api-version=$script:apiVersion"
                    # headers
                    $AcessToken = Get-AccessToken
                    $requestHeader = @{
                      "Authorization" = "Bearer {0}" -f $AcessToken
                      "Content-Type" = "application/json"
                    }

                    $isDSCConfig = Invoke-WebRequest -Uri $uri -Headers $requestHeader -Method Get -OutFile -ErrorAction Stop -UseBasicParsing
                    
                }
                catch [System.Net.WebException]
                {
                    $configNotFound = $true
                }
                    
                if ($isDSCConfig)
                {
                    try
                    {
                        # We know file is a DSC configuration.
                        Write-Tracing -Level Informational -Message "Deleting via Remove-AzureRmAutomationDscConfiguration."

                        # Request uri.
                        $uri = "$script:apiEndpoint/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.Automation/automationAccounts/$automationAccountName/configurations/$fileName`?api-version=$script:apiVersion"
                        # headers
                        $AcessToken = Get-AccessToken
                        $requestHeader = @{
                          "Authorization" = "Bearer {0}" -f $AcessToken
                          "Content-Type" = "application/json"
                        }

                        $null = Invoke-WebRequest -Uri $uri -Headers $requestHeader -Method Delete -ErrorAction Stop
                        
                        Write-Tracing -Level Succeeded -Message "DSC configuration '$file' successfully deleted."
                        $successfulDelete = $true
                    }
                    catch [System.Net.WebException]
                    {
                        $deleteError = $true

                        $reason = "FailedToDelete"
                        $errorMessage = New-ErrorMessage -ExceptionThrown $_ -Message "Failed to delete '$file'."
                        Write-ErrorRecord -ErrorId $reason `
                                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                                          -Exception ([System.InvalidOperationException]::New($errorMessage)) `
                                          -Type NonTerminatingError
                    }
                }
            }
        }

        if ($successfulDelete -or ($Mode -eq "LocalDevelopment"))
        {
            $deletedRunbooks.Add($file)
        }
        else
        {
            # File was not found in user's Azure Automation account.
            if ($runbookNotFound -and $configNotFound)
            {
                Add-FileToSkip -FileName $file -Reason NotFoundInAutomationAccount
            }
            # There was an error deleting the file.
            elseif ($deleteError)
            {
                Add-FileToSkip -FileName $file -Reason FailedToDelete
            }
        }
    }

    if (($script:skippedFiles.Count -gt 0) -or ($importedRunbooks.Count -gt 0) -or ($deletedRunbooks.Count -gt 0))
    {
        Write-Summary -SkippedFiles $script:skippedFiles -ImportedRunbooks $importedRunbooks -DeletedRunbooks $deletedRunbooks
    }
    else
    {
        $message = "There are no runbooks in this folder path to sync."
        Write-Tracing -Level Informational -Message $message -DisplayMessageToUser 
    }
}
finally
{
    if ($psFolderPath -and (Test-Path $psFolderPath))
    {
        Write-Tracing -Level Informational -Message "Deleting temp directory '$psFolderPath'."

        # TODO: 149951: Ensure temp folder where the runbooks are downloaded to in the Sandbox is deleted after syncing.
        
        # There is a known issue for remove-item with -recurse which does work as intented. 
        # To workaround this, we use get-childitem and remove-item together.

        # Delete all the files first.
        Get-ChildItem $psFolderPath -Recurse -ErrorAction SilentlyContinue | 
            Remove-Item -Force -ErrorAction SilentlyContinue

        # Delete the folder.
        Remove-Item $psFolderPath -Force -ErrorAction SilentlyContinue
    }
}

Write-Tracing -Level Informational -Message "Script execution completed."