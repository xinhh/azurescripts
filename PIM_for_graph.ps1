

$clientID = "<AppID>" # app registration clientId
$clientSecret = "<AppSecret>" # app registration client secret
$tenantID = "<your_tenantId>" # tenant ID

$Scope = "https://microsoftgraph.chinacloudapi.cn/.default"
$PrincipalId = "<your_account_ID>"
$RequestType="adminAssign"
$Justification="PIM assignment"
$roleDefinitionId= "c430b396-e693-46cc-96f3-db01bf8bb62a" # aad role definition ID

$duration = "PT3H" # Use ISO 8601 format; P1D = 24 hours, PT3H = 3 hours which is the default value
$ExpirationType="afterDuration"  # // Values: afterDuration, afterDateTime, NoExpiration

$Body = @{
    Grant_Type = "client_credentials"  
    Scope      =  $Scope
    client_Id = $clientID    
    client_secret  = $clientSecret 
}

$authUri = "https://login.chinacloudapi.cn/$tenantID/oauth2/v2.0/token"
  
$TokenResponse = Invoke-RestMethod -Uri $authUri -Method POST -Body $Body 
$token= $($TokenResponse.access_token)

Connect-MgGraph -AccessToken $token -Environment China
Select-MgProfile -Name "beta"
$startdate = $(Get-Date  ([datetime]::UtcNow) -format s).tostring()+"Z"


$body = @"
{
    "action": "$RequestType",
    "justification": "$Justification",
    "roleDefinitionId": "$roleDefinitionId",
    "directoryScopeId": "/",
    "principalId": "$PrincipalId",
    "scheduleInfo": {
        "startDateTime": "$startdate",
        "expiration": {
            "type": "$ExpirationType", 
            "endDateTime": null,
            "duration": "$duration"
        }
    }
}
"@

New-MgRoleManagementDirectoryRoleAssignmentScheduleRequest -BodyParameter $body