[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $paths, # seperated with comma 

    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [string]
    $user="BUILTIN\Remote Desktop Users",

    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [switch]
    $clearonly
)

function set-folderacl ($path) {

    # Set new rule properties
    $identity = $user
    $fileSystemRights = "DeleteSubdirectoriesAndFiles, Write, Delete, ChangePermissions, TakeOwnership"
    $InheritanceFlags = "ContainerInherit, ObjectInherit"
    $type = "Deny"
    $flag="None"
    $rule = New-Object System.Security.AccessControl.FileSystemAccessRule($identity, $fileSystemRights, $InheritanceFlags,$flag, $type) 

    # update acl
    $NewAcl = Get-Acl -Path $path
    $NewAcl.SetAccessRule($rule)
    write-host "update ACL for $path with $user"
    Set-Acl -Path $path -AclObject $NewAcl

}

function clear-folderacl ($path) {
    # remove the ACL rule properties for remote desktop users
    write-host "clear ACL for $path with $user"
    icacls $path /c /q /remove $user
}


foreach ($path in $paths.split(",")) {
    
    if ($clearonly) {
        clear-folderacl $path
    } else {
        clear-folderacl $path
        set-folderacl $path
    }
}