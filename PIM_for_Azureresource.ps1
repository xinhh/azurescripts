


# referring: https://docs.microsoft.com/en-us/rest/api/authorization/privileged-role-assignment-rest-sample

# sample scope for assignment: 
# $subscriptionId = "0f2daa80-6b16-44ee-8016-4ad888e059ac"
# $scope = "subscriptions/$subscriptionId"

# to get the az role definition ID of a az role like backup operator: get-azroledefinition -Name "backup operator"

# all available supported requested type
# AdminAssign
# AdminRemove
# AdminUpdate
# AdminExtend
# AdminRenew
# SelfActivate
# SelfDeactivate
# SelfExtend
# SelfRenew

# sample usage to create a new assignment
# $PrincipalId = "<your_account_ID>" 
# $RoleDefinitionId = "<Azure Role ID>"
# $subscriptionId = "<Subscription_ID>"
# $scope = "subscriptions/$subscriptionId"
# ./PIM_for_Azureresource.ps1 -PrincipalId $PrincipalId -subscriptionID  $subscriptionId -RoleDefinitionId $RoleDefinitionId -scope $scope



param (
[Parameter(Mandatory=$false)][string]$resourceuri="https://management.chinacloudapi.cn/",
[Parameter(Mandatory=$false)][string]$Duration="PT3H",  # Use ISO 8601 format; P1D = 24 hours, PT3H = 3 hours which is the default value
[Parameter(Mandatory=$false)][string]$ExpirationType="afterDuration",  # // Values: afterDuration, afterDateTime, NoExpiration
[Parameter(Mandatory=$false)][string]$Justification="PIM assignment",  # need justiication for assignment
[Parameter(Mandatory=$false)][string]$RequestType="AdminAssign",  # need justiication for assignment
[Parameter(Mandatory=$false)][string]$roleAssignmentScheduleRequestName="",  # assignement request ID, if not provided, will create a new GUID for action like create a new request
[Parameter(Mandatory=$true)][string]$scope,
[Parameter(Mandatory=$true)][string]$subscriptionId,
[Parameter(Mandatory=$true)][string]$PrincipalId, # user object ID or service principal ID
[Parameter(Mandatory=$true)][string]$RoleDefinitionId # role definition ID
)

# skip login if you have already have an active context
Connect-AzAccount -Environment AzureChinaCloud -Subscription $subscriptionId -SkipContextPopulation

# grant resource token
$token = Get-AzAccessToken -ResourceUrl $resourceuri

if ($roleAssignmentScheduleRequestName -eq "") {
  $roleAssignmentScheduleRequestName = $([guid]::NewGuid()).tostring()
} 

# set the start date as the current UTC time
$startdate = $(Get-Date  ([datetime]::UtcNow) -format s).tostring()+"Z"

# build REST API URI

$uri="$resourceuri$($scope)/providers/Microsoft.Authorization/roleAssignmentScheduleRequests/$($roleAssignmentScheduleRequestName)?api-version=2020-10-01"

# build REST API header
$headers = @{
    "Authorization" = "Bearer $($token.Token)";
}

# build REST API payload object
$payload = @"
{
    "Properties": {
      "RoleDefinitionId": "/subscriptions/$subscriptionId/providers/Microsoft.Authorization/roleDefinitions/$roleDefinitionId",
      "PrincipalId": "$PrincipalId",
      "RequestType": "$RequestType",
      "Justification": "$Justification",
      "ScheduleInfo": {
        "StartDateTime": "$($startdate)",
        "Expiration": {
          "type": "$ExpirationType",
          "endDateTime": null,
          "duration": "$Duration"
        }        
      }
    }
"@

$result = Invoke-WebRequest -UseBasicParsing -Method PUT -Headers $headers -Uri $uri -Body $payload -ContentType "application/json"

if ($result.StatusCode -le 201 -and $NULL -ne $result.StatusCode ) {
  $s=$($result.content | convertfrom-json).properties
  write-host "assigement $($s.targetRoleAssignmentScheduleId) is summited with a status $($s.status)"
} else {
  write-host $result.rawcontent
}

