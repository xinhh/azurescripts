<#
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the MIT License.
#>
 
[Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseApprovedVerbs", "")]
param(
    [Parameter(Mandatory = $true)]
    [string] $ResourceGroupName,
 
    [Parameter(Mandatory = $true)]
    [string] $AutomationAccountName,

    [Parameter(Mandatory = $true)]
    [string] $AutomationAccountSubID,
 
    [string] $AzureEnvironmentName = 'AzureChinaCloud'

)
 
$ErrorActionPreference = "Continue"
 
#region Constants
#use MSI identity
$script:Automation_Sourcecontrol_Auth_Mechanism = "System-Assigned Managed Identity"


# API version.
$script:apiVersion = "2015-10-31"
$script:apiEndpoint = $null

# Active Directory for AzureEnvironement
$script:AzureEnvironmentToActiveDirectory = @{
    DogFood = "https://login.windows-ppe.net"
    AzureCloud = "https://login.microsoftonline.com"
    AzureGermanCloud = "https://login.microsoftonline.de"
    AzureChinaCloud = "https://login.chinacloudapi.cn"
    AzureUSGovernment = "https://login.microsoftonline.us"
}

# endpoint based on AzureEnvironmentName
$script:AzureEnvironmentToResourceManagerURL = @{
    DogFood = "https://df.onecloud.azure-test.net"
    AzureCloud = "https://management.azure.com"
    AzureGermanCloud = "https://management.microsoftazure.de"
    AzureChinaCloud = "https://management.chinacloudapi.cn"
    AzureUSGovernment = "https://management.usgovcloudapi.net"
}

# Use the Run As connection to login to Azure

function Write-ErrorRecord
{
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [String]
        $ErrorId,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $ErrorCategory,

        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        $Exception,

        [Parameter(Mandatory=$false)]
        [ValidateSet("TerminatingError", "NonTerminatingError", IgnoreCase = $true)]
        [String]
        $Type = "TerminatingError"
    )
    $script:ReasonToNotStartSyncJob = $Exception.Message
    $errorRecord = [System.Management.Automation.ErrorRecord]::New($Exception, $ErrorId, $ErrorCategory, $null)
    Write-Error -ErrorRecord $errorRecord
    if ($Type -eq "TerminatingError")
    {
        throw $errorRecord
    }
}

function Get-ManagedIdentityAccessToken
{
    Param
    (
        [Parameter(Mandatory=$false)]
        [String]
        $ClientId
    )
    $resource = "?resource=$script:apiEndpoint"
    $url = $env:IDENTITY_ENDPOINT + $resource
    if($ClientId)
    {
        $url += "&client_id=$ClientId"
    }
    $Headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]" 
    $Headers.Add("Metadata", "True")
    $Headers.Add("X-IDENTITY-HEADER", $env:IDENTITY_HEADER)
    $Response = Invoke-RestMethod -Uri $url -Method 'GET' -Headers $Headers -ErrorAction Stop
    $token = $Response.access_token
    return $token
}

function Get-ResourceUri
{

    Param
    (
        [Parameter(Mandatory=$false)]
        [String]
        $operation
    )
    if($operation) {
        $uri="$script:apiEndpoint/subscriptions/$AutomationAccountSubID/resourceGroups/$ResourceGroupName/providers/Microsoft.Automation/automationAccounts/$AutomationAccountName/$operation`?api-version=$script:apiVersion"
    } else {
        $uri="$script:apiEndpoint/subscriptions/$AutomationAccountSubID/resourceGroups/$ResourceGroupName/providers/Microsoft.Automation/automationAccounts/$AutomationAccountName`?api-version=$script:apiVersion"
    }
    return $uri
}

function Set-ResourceManagerEndpoint
{
    $azureEnvironment = $script:AzureEnvironmentToResourceManagerURL[$AzureEnvironmentName]
    $script:apiEndpoint = $azureEnvironment

    if (-not $script:apiEndpoint)
    {
        $exception = [System.InvalidOperationException]::New("Unable to get 'ResourceManagerUrl' from AzureRmEnvironment")
        Write-ErrorRecord -ErrorId "UnableToRetrieveResourceManagerUrl" `
                          -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
                          -Exception $exception
    }
}


# build Apiendpoint due to Azure Environment
Set-ResourceManagerEndpoint 

$AcessToken = Get-ManagedIdentityAccessToken
$headers = @{
    "Authorization" = "Bearer {0}" -f $AcessToken
    "Content-Type" = "application/json"
}

# try to get the current built-in powershell modules
$listmoduleuri = Get-ResourceUri -operation "listbuiltinmodules"
$response = Invoke-WebRequest -Method POST -Uri $listmoduleuri -Headers $headers -ErrorAction Stop -UseBasicParsing

$powershellmodules = $response.content | convertfrom-json
$latestpowershellazversion = $powershellmodules.powershell.Az | sort-object [version]$_ -Descending | select -first 1
$latestpowershell7azversion = $powershellmodules.powershell7.Az | sort-object [version]$_ -Descending | select -first 1

# get the current Automation account builtin modules 

$uri = Get-ResourceUri 
$response = Invoke-WebRequest -Method GET -Uri $uri -Headers $headers -ErrorAction Stop -UseBasicParsing
$automationaccount = $response.content | convertfrom-json

$currentpowersehllazversion = $automationaccount.properties.RuntimeConfiguration.powershell.builtinModules.Az
$currentpowersehll7azversion = $automationaccount.properties.RuntimeConfiguration.powershell7.builtinModules.Az

# if version is not up-to-date, try to update to latest buildin Az version
if ($currentpowersehllazversion -ne $latestpowershellazversion -or $currentpowersehll7azversion -ne $latestpowershell7azversion) {
    Write-Output "detect powershell Az module is not up-to-date"
    Write-Output "Current Powershell Az: $currentpowersehllazversion; Latest Powersehll Az: $latestpowershellazversion"
    Write-Output "Current Powershell 7 Az: $currentpowersehll7azversion; Latest Powersehll 7 Az: $latestpowershell7azversion"
    $RuntimeConfiguration = [PSCustomObject]@{
        "powershell"=[PSCustomObject]@{
            "builtinModules" = [PSCustomObject]@{
                "Az" = "$latestpowershellazversion"
            }
        }
        "powershell7"=[PSCustomObject]@{
            "builtinModules" = [PSCustomObject]@{
                "Az" = "$latestpowershell7azversion"
            }
        }
    }

    $body = @{
        "properties" = [PSCustomObject]@{
            "RuntimeConfiguration" = $RuntimeConfiguration 
       }
    } | convertto-json -depth 10

 
    # Try to update automation Az builtin module 
    try
    {
        Write-Output "Update automation Az"
        $updateresult = Invoke-WebRequest -UseBasicParsing -Method PATCH -Headers $headers -Uri $uri -Body $body -ContentType "application/json"
        $newpowershellmodules = $updateresult.content | convertfrom-json

        $newpowersehllazversion = $newpowershellmodules.properties.RuntimeConfiguration.powershell.builtinModules.Az
        $newpowersehll7azversion = $newpowershellmodules.properties.RuntimeConfiguration.powershell7.builtinModules.Az

        write-output "Powershell Az Module updated from  $currentpowersehllazversion to $newpowersehllazversion"
        write-output "Powershell 7 Az Module updated from  $currentpowersehll7azversion to $newpowersehll7azversion"
        
    } catch [System.Net.WebException] {
        $statusCode = $_.Exception.Response.StatusCode.value__
        $statusDescription = $_.Exception.Response.StatusDescription
        $message = $null
        If ($statusDescription)
        {
            $message = "WebRequest failed to get all runbooks for automation account with $statusDescription($statusCode)"
        }
        else
        {
            $message = "WebRequest failed to get all runbooks for automation account with status code: $statusCode"
        }
        Write-ErrorRecord -ErrorId "UnableToUpdateAutomationAzruntimemodule" `
        -ErrorCategory ([System.Management.Automation.ErrorCategory]::InvalidOperation) `
        -Exception $message
    }
            
} else {
    Write-Output "skip update since All powershell Az module are up-to-date"
}

