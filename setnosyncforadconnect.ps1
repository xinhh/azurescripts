# sample usage 
# $searchbase="OU=digiou,OU=testOU,DC=idgmcaad001,DC=net"
# $groups="devgroup,test"
# ./setnosyncforadconnect.ps1 -searchbase $searchbase -group $groups

param (
    [Parameter(Mandatory=$false)][string]$searchbase="",
    [Parameter(Mandatory=$true)][string]$groups,
    [Parameter(Mandatory=$false)][string]$aaduserpropertyname="msDS-cloudExtensionAttribute8",
    [Parameter(Mandatory=$false)][string]$aaduserpropertyvalue="avd",
    [Parameter(Mandatory=$false)][int]$maxnestlevel=2
    )


# nested functions
function checkgroupmembers {
    param (
        [Parameter(Mandatory=$false)][string]$searchbase,
        [Parameter(Mandatory=$true)][string]$group,
        [Parameter(Mandatory=$true)][int]$nestlevel,
        [Parameter(Mandatory=$false)][string]$aaduserpropertyname,
        [Parameter(Mandatory=$false)][string]$aaduserpropertyvalue
    )

    $nextnestlevel = $nestlevel+1
    $global:excludefilterstring = $global:excludefilterstring+" -and [string]`$_.memberOf -NotMatch `"CN=$group,`"" 
    $includemembers = $(get-adgroup -identity $group -property members).members
    foreach ($includemember in $includemembers) {

        $adobj = Get-ADobject -Identity $includemember | where {$_.distinguishedname -match $searchbase}

        # only mark sync flag on max nested group member
        if ($adobj.ObjectClass -eq 'user') {
            $userobj = Get-ADUser -Identity $includemember -Properties $aaduserpropertyname  | where {$_.distinguishedname -match $searchbase}
                 if ($userobj.($aaduserpropertyname) -ne $aaduserpropertyvalue -and $NULL -ne $userobj) {
                    $global:needdeltasync = $true
                    write-host "add avd signal to allow user $($userobj.Name) to sync in AAD connector" 
                    $cmd="Set-ADUser -identity `"$includemember`" -replace @{`"$($aaduserpropertyname)`"=`"$aaduserpropertyvalue`"}"
                    Invoke-Expression $cmd
                }
        } else {
        # loop child group; skip if the nest level is great than max nest level
            if ($nextnestlevel -lt $maxnestlevel) {
                checkgroupmembers -searchbase $searchbase -group $includemember -nest $nextnestlevel -aaduserpropertyname $aaduserpropertyname -aaduserpropertyvalue $aaduserpropertyvalue
                $groupname = $includemember.split(",=")[1].tostring()
                $global:excludefilterstring = $global:excludefilterstring+" -and [string]`$_.memberOf -NotMatch `"CN=$groupname,`""            
            }
        }

    }
}

# step 0: build exclude string

Import-Module ActiveDirectory
# build OU search base
$sgroups = $groups.split(",")
$global:excludefilterstring = "`$_.`"$aaduserpropertyname`" -eq `"$aaduserpropertyvalue`""
$global:needdeltasync = $false

if ($searchbase -eq "") {
    $domain = Get-ADDomain -Current LocalComputer
    $searchbase=$domain.DistinguishedName
}

# step 1: add avd signal to target group members
foreach ($group in $sgroups) {
    write-host "enum group membership of group $group"
    checkgroupmembers -searchbase $searchbase -group $group -nest 0 -aaduserpropertyname $aaduserpropertyname -aaduserpropertyvalue $aaduserpropertyvalue
}

# step 2: remove avd signal if it is not a members of target groups
$command = "Get-ADUser -Filter * -SearchBase `"$searchbase`" -SearchScope Subtree -Properties distinguishedname, MemberOf,$aaduserpropertyname | Where {$($global:excludefilterstring)}"
$excluderusers = Invoke-Expression $command
foreach ($excluderuser in $excluderusers) {
    if ($excluderuser.$($aaduserpropertyname) -eq $aaduserpropertyvalue) {
    $cmd="Set-ADUser -identity `"$($excluderuser.DistinguishedName)`" -clear `"$($aaduserpropertyname)`""
    write-host "remove avd for user $($excluderuser.DistinguishedName) to disable aad user sync" 
    $global:needdeltasync = $true
    Invoke-Expression $cmd
    }
}


# step 3: trigger delta sync

if ($global:needdeltasync) { 
    write-host "detect new membership added, trigger a delta AD connector sync"
    Import-Module ADSync
    Start-ADSyncSyncCycle -PolicyType Delta
}