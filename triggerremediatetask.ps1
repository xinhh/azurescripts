param (
[Parameter(Mandatory=$true)][string]$SubscriptionId, 
[Parameter(Mandatory=$false)][string]$AssignmentId,
[Parameter(Mandatory=$false)][int]$topcount=100,
[Parameter(Mandatory=$false)][string]$DiscoveryMode='ExistingNonCompliant'   # ReEvaluateCompliance or ExistingNonCompliant
)


Import-Module Az.Accounts
Import-Module Az.PolicyInsights

$AzureContext = (Connect-AzAccount -EnvironmentName AzureChinaCloud -Identity -Subscription $SubscriptionId).context

if ($AzureContext) {
    # check if non-complaint resource existing
    if ($AssignmentId) {
        $noncomplaintrules = Get-AzPolicyState -subscription $SubscriptionId -Filter "ComplianceState eq 'NonCompliant' AND PolicyAssignmentId eq '$AssignmentId' AND (PolicyDefinitionAction eq 'deployifnotexists' or PolicyDefinitionAction eq 'modify')" -Apply "groupby((PolicyAssignmentId, PolicySetDefinitionId, PolicyDefinitionReferenceId, PolicyDefinitionId,PolicyDefinitionAction, PolicyAssignmentScope, ResourceId))/groupby((PolicyAssignmentId, PolicySetDefinitionId, PolicyDefinitionReferenceId,PolicyDefinitionAction, PolicyAssignmentScope, PolicyDefinitionId), aggregate(`$count as NumNonCompliantResources))"
    } else {    
        $noncomplaintrules = Get-AzPolicyState -subscription $SubscriptionId -Filter "ComplianceState eq 'NonCompliant' AND (PolicyDefinitionAction eq 'deployifnotexists' or PolicyDefinitionAction eq 'modify')" -Apply "groupby((PolicyAssignmentId, PolicySetDefinitionId, PolicyDefinitionReferenceId, PolicyDefinitionId,PolicyDefinitionAction, PolicyAssignmentScope, ResourceId))/groupby((PolicyAssignmentId, PolicySetDefinitionId, PolicyDefinitionReferenceId,PolicyDefinitionAction, PolicyAssignmentScope, PolicyDefinitionId), aggregate(`$count as NumNonCompliantResources))"
    }
    
    foreach ($noncomplaintrule in $noncomplaintrules) {
        write-host "$noncomplaintrule"

        $PolicyAssignmentId = $noncomplaintrule.PolicyAssignmentId
        $PolicyDefinitionReferenceId = $noncomplaintrule.PolicyDefinitionReferenceId
        $scope = $noncomplaintrule.PolicyAssignmentScope

        if ($PolicyDefinitionReferenceId) {
            write-host "found non-complaint policy state for $PolicyDefinitionReferenceId"
            $remediationtask = Get-AzPolicyRemediation -scope $scope -Filter "PolicyAssignmentId eq '$PolicyAssignmentId' AND PolicyDefinitionReferenceId eq '$PolicyDefinitionReferenceId'" | Sort-Object -Property LastUpdatedOn -Descending | select -first 1
        } else {
            write-host "found non-complaint policy state for $PolicyAssignmentId"
            $remediationtask = Get-AzPolicyRemediation -scope $scope -Filter "PolicyAssignmentId eq '$PolicyAssignmentId'" | Sort-Object -Property LastUpdatedOn -Descending | select -first 1
        }

        if ($remediationtask) {
            $taskname = $($remediationtask.name)
            # skip if the remediation task is not completed
            if (($remediationtask.ProvisioningState -eq 'Succeeded') -or ($remediationtask.ProvisioningState -eq 'Failed')) {
                write-host "start remediate task $taskname"
                if ($PolicyDefinitionReferenceId) {  
                    Start-AzPolicyRemediation -scope $scope -PolicyAssignmentId $policyAssignmentId -PolicyDefinitionReferenceId $PolicyDefinitionReferenceId -Name $taskname -ResourceDiscoveryMode $DiscoveryMode -ResourceCount $topcount 
                } else {
                    Start-AzPolicyRemediation -scope $scope -PolicyAssignmentId $policyAssignmentId -Name $taskname -ResourceDiscoveryMode $DiscoveryMode -ResourceCount $topcount 
                }
            }

        } else {
            $taskname =  $(new-guid).tostring()

            write-host "start remediate task $taskname"
            if ($PolicyDefinitionReferenceId) {  
                    Start-AzPolicyRemediation -scope $scope -PolicyAssignmentId $policyAssignmentId -PolicyDefinitionReferenceId $PolicyDefinitionReferenceId -Name $taskname -ResourceDiscoveryMode $DiscoveryMode -ResourceCount $topcount 
            } else {
                    Start-AzPolicyRemediation -scope $scope -PolicyAssignmentId $policyAssignmentId -Name $taskname -ResourceDiscoveryMode $DiscoveryMode -ResourceCount $topcount 
            }
        }
    }

}