
$message = "";
function Backup-IssuanceTransformRules {
	$currentpath = Resolve-Path .
	$timestamp = Get-Date -f yyyy.MM.dd_hh.mm.ss;
	$filename = "$currentpath\Backup $timestamp.txt";
	$claims = Get-AdfsRelyingPartyTrust -Identifier $(Get-RpIdentifier) | Select-Object IssuanceTransformRules;
	$stream = [System.IO.StreamWriter] $filename
	foreach($claim in $claims.IssuanceTransformRules) {
		$stream.WriteLine($claim);
	}
	$stream.close();
	$script:message = "Backup file with claim rules created: $filename";
}

function Get-RpIdentifier {
	[string]$rpidentifier = "";
	$identifiers = Get-AdfsRelyingPartyTrust | Select-Object Identifier;
	foreach($id in $identifiers.Identifier) {
		if($id.IndexOf("microsoftonline", [System.StringComparison]::OrdinalIgnoreCase) -ge 0 -and $id.StartsWith("urn", [System.StringComparison]::OrdinalIgnoreCase)) {
			$rpidentifier = $id;
			break;
		}
		
	}

	if([string]::IsNullOrEmpty($rpidentifier)) {
		throw "Unable to get the ADFS relying party trust identifier.";
	}

	return $rpidentifier;
}

function Update-AdfsClaimRules {
$RuleSet = New-AdfsClaimRuleSet -ClaimRule "@RuleName = `"Issue UPN`"
c:[Type == `"http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname`"]
 => issue(store = `"Active Directory`", types = (`"http://schemas.xmlsoap.org/claims/UPN`"), query = `"samAccountName={0};mail;{1}`", param = regexreplace(c.Value, `"(?<domain>[^\\]+)\\(?<user>.+)`", `"`${user}`"), param = c.Value);
","@RuleName = `"Issue Immutable ID`"
c:[Type == `"http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname`"]
 => issue(store = `"Active Directory`", types = (`"http://schemas.microsoft.com/LiveID/Federation/2008/05/ImmutableID`"), query = `"samAccountName={0};objectGUID;{1}`", param = regexreplace(c.Value, `"(?<domain>[^\\]+)\\(?<user>.+)`", `"`${user}`"), param = c.Value);
","@RuleName = `"Issue nameidentifier`"
c:[Type == `"http://schemas.microsoft.com/LiveID/Federation/2008/05/ImmutableID`"]
 => issue(Type = `"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier`", Value = c.Value, Properties[`"http://schemas.xmlsoap.org/ws/2005/05/identity/claimproperties/format`"] = `"urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified`");
","@RuleName = `"Issue accounttype for domain-joined computers`"
c:[Type == `"http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid`", Value =~ `"-515`$`", Issuer =~ `"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)`$`"]
 => issue(Type = `"http://schemas.microsoft.com/ws/2012/01/accounttype`", Value = `"DJ`");
","@RuleName = `"Issue AccountType with the value USER when it is not a computer account`" NOT EXISTS([Type == `"http://schemas.microsoft.com/ws/2012/01/accounttype`", Value == `"DJ`"])
 => add(Type = `"http://schemas.microsoft.com/ws/2012/01/accounttype`", Value = `"User`");
","@RuleName = `"Issue issuerid when it is not a computer account`" c1:[Type == `"http://schemas.xmlsoap.org/claims/UPN`"] && c2:[Type == `"http://schemas.microsoft.com/ws/2012/01/accounttype`", Value == `"User`"] => issue(Type = `"http://schemas.microsoft.com/ws/2008/06/identity/claims/issuerid`", Value = regexreplace(c1.Value, `"(?i)(^([^@]+)@)(?<domain>(sxaiplabcn\.net))`$`", `"http://`${domain}/adfs/services/trust/`"));
","@RuleName = `"Issue issuerid for DJ computer auth`" c1:[Type == `"http://schemas.microsoft.com/ws/2012/01/accounttype`", Value == `"DJ`"] => issue(Type = `"http://schemas.microsoft.com/ws/2008/06/identity/claims/issuerid`", Value =`"http://sxaiplabcn.net/adfs/services/trust/`");
","@RuleName = `"Issue onpremobjectguid for domain-joined computers`"
c1:[Type == `"http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid`", Value =~ `"-515`$`", Issuer =~ `"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)`$`"]
 && c2:[Type == `"http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname`", Issuer =~ `"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)`$`"]
 => issue(store = `"Active Directory`", types = (`"http://schemas.microsoft.com/identity/claims/onpremobjectguid`"), query = `";objectguid;{0}`", param = c2.Value);
","@RuleName = `"Pass through primary SID`"
c1:[Type == `"http://schemas.microsoft.com/ws/2008/06/identity/claims/groupsid`", Value =~ `"-515`$`", Issuer =~ `"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)`$`"]
 && c2:[Type == `"http://schemas.microsoft.com/ws/2008/06/identity/claims/primarysid`", Issuer =~ `"^(AD AUTHORITY|SELF AUTHORITY|LOCAL AUTHORITY)`$`"]
 => issue(claim = c2);
","@RuleName = `"Pass through claim - insideCorporateNetwork`"
c:[Type == `"http://schemas.microsoft.com/ws/2012/01/insidecorporatenetwork`"]
 => issue(claim = c);
","@RuleName = `"Pass Through Claim - Psso`"
c:[Type == `"http://schemas.microsoft.com/2014/03/psso`"]
 => issue(claim = c);
","@RuleName = `"Issue Password Expiry Claims`"
c1:[Type == `"http://schemas.microsoft.com/ws/2012/01/passwordexpirationtime`"]
 => issue(store = `"_PasswordExpiryStore`", types = (`"http://schemas.microsoft.com/ws/2012/01/passwordexpirationtime`", `"http://schemas.microsoft.com/ws/2012/01/passwordexpirationdays`", `"http://schemas.microsoft.com/ws/2012/01/passwordchangeurl`"), query = `"{0};`", param = c1.Value);
","@RuleName = `"Pass Through Claim - AlternateLoginID`"
c:[Type == `"http://schemas.microsoft.com/ws/2013/11/alternateloginid`"]
 => issue(claim = c);
","@RuleName = `"Pass through claim - authnmethodsreferences`"
c:[Type == `"http://schemas.microsoft.com/claims/authnmethodsreferences`"]
 => issue(claim = c);
","@RuleName = `"Pass through claim - multifactorauthenticationinstant`"
c:[Type == `"http://schemas.microsoft.com/ws/2017/04/identity/claims/multifactorauthenticationinstant`"]
 => issue(claim = c);
","@RuleName = `"Pass through claim - certificate authentication - serial number`"
c:[Type == `"http://schemas.microsoft.com/ws/2008/06/identity/claims/serialnumber`"]  => issue(claim = c);
","@RuleName = `"Pass through claim - certificate authentication - issuer`"
c:[Type == `"http://schemas.microsoft.com/2012/12/certificatecontext/field/issuer`"]  => issue(claim = c);
"
Backup-IssuanceTransformRules;
Set-AdfsRelyingPartyTrust -TargetIdentifier $(Get-RpIdentifier) -IssuanceTransformRules $RuleSet.ClaimRulesString;
Write-Host $message;
Write-Host Successfully updated AD FS claim rules;
}
Update-AdfsClaimRules;

